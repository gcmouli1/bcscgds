/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimiztion
*  author: Gannaravarapu Chandramouli
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <time.h>
#include<stdbool.h> /* use C99 or above versions */
#ifndef _FUNCEVAL_
#define _FUNCEVAL_
#ifndef INFS
#define INFS DBL_MAX
#endif
#ifndef MAX
#define MAX(a,b) ((a) > (b) ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? a : b)
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

typedef struct
{
	int dimension;
	int numConstr;
	int iterLimit;
	double precision;
	char *pollPattern;
	char p;
	bool initPtFeasibility;
	double *startPt;
	FILE *fpBound;
	int logLevel;
} userData;

//#include "vecLapack.h"
//#include "madsDeclare.h"
//double funcEvalValue (vectorD *point, madsStructure *mads);
//void constrEval (vectorD *x, madsStructure *mads);
void userBasicIntialize (userData *uData, char **argv);
void userExtraIntialize (userData *uData, char **argv);
double funcEvalValue(double *x, userData *uData);
void constrEval (double *x, double *constrVec, userData *uData);
void boundsEval (double *varLB, double *varUB, double *constrLB, double *constrUB, userData *uData);
#endif
