#
#This file is a part of "bcscgds".
#"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
#
#
#
# bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
# Solver for Bound Constrained Derivative Free Optimization
# author: Gannaravarapu Chandramouli




import numpy as np
import math as math
x=np.loadtxt("inputPoint.txt")
funcval=0
dimension=len(x)
for i in range(dimension-1):
    funcval += 100*math.pow(x[i+1]-math.pow(x[i],2),2)+math.pow(x[i]-1,2)

f = open("funcOutput.txt","w")
f.write(str(funcval))



