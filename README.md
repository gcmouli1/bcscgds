Bound Constrained Scaled Conjugate Gradient based Direct Search (bcscgds) for solving derivative free optimization with bound constraints.

This algorithm solves bound constrained optimization problems without any derivative/gradient information. 
It is written completely in C with only external dependencies of blas and lapack.

Please cite the following paper if you are planning to use this software:

Gannavarapu Chandramouli and Vishnu Narayanan, "A scaled conjugate gradient based direct search algorithm for 
high dimensional box constrained derivative free optimization" [arXiv:1901.05215].


Make sure you have blas and lapack installed on your system. for e.g. on ubuntu,mint,debian etc this can done by

**$sudo apt-get install libblas-dev liblapack-dev**


To run the algorithm, do following steps:

1. Goto the directory BCSCG-DS. If this is first time then simply run **$make** to create a static library libbcscgds.a
2. Make changes to the funcEval.c file to define your function and run **$make bcscgds**
3. Now simply run **$./bcscgds parameters.txt**

Options are given in parameters.txt file. One can use arguments other than regular options given in parameters.txt

We assume that libbcscgds.a has been created using **$make** command.
For working with examples given in Example directory:

   1. Copy the files to current folder with filename changed to funcEval.c (create backup).\
      **$mv funcEval.c funcEval.c_bak $cp Examples/evenDimRound.c funcEval.c**
   2. Create the executable **$make bcscgds**
   3. Run the program  **$./bcscgds parameters.txt**

The Bounds.dat file contain lower and upper bounds for each variable while a.dat contains initial point information. In parameters.txt file:

1. Dimension option should always be at top.
2. Currently, the code works only for bound constrained problems. So set NumberConstraints option to 0.
3. Precision option gives the tolerance.
4. InitialPointFeasible should be 1 (true). i.e. startPt.dat should contain point feasible for bound constraints.
5. Maxevals option sets the maximum number of function evaluations allowed for the solver.
6. Loglevel option gives degree of output information. for e.g. 0 gives no extra information, 1 gives the set of all feasible solutions computed along various iterations, and 2 gives set of feasible solutions as well as feasible points generated by various iterations. The output is in CSV format.

