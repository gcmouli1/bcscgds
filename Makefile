
#This file is a part of "bcscgds".
#"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
#
#
#
#  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
#  Solver for Bound Constrained Derivative Free Optimiztion
#  author: Gannaravarapu Chandramouli



EXE = testing
SOURCEPATH = $(shell pwd)
SRC = $(SOURCEPATH)/SRC
OBJDIR = $(SOURCEPATH)/Objects
OBJS_tmp = vecLapack.o vecBlas.o vecMatDeclare.o quadLinearHybrid.o quadInterpolate.o quadraticFunc.o RemoveDuplicate.o  madsDeclare.o  bracketing.o singlePtEval.o Brent.o lineSearch.o simplexGradient.o quickSort.o vicinitySearch.o pollStep.o Halton.o primeCollect.o quadHessianPositivity.o funcConstrSync.o testing.o 

OBJS = $(patsubst %.o,$(OBJDIR)/%.o, $(OBJS_tmp))
STATICLIBRARY = libbcscgds.a

OBJFUNCEVAL = funcEval.o
TESTING = testing.o funcEval.o 

LIBPATH = $(SOURCEPATH)/LIBS
INCLUDEDIR = 
LIBRARYPATH += -L $(LIBPATH)

# C Compiler command
CC = gcc 
FC = gfortran

# C Compiler options
CFLAGS = -O3 -fPIC
FORTRANFLAG = -lgfortran
MADSFLAG = -lbcscgds

# additional C Compiler options for linking
LFLAGS = -llapack -lblas -lm -Wl,-rpath=$(LIBPATH)

STATICLIBRARY : $(OBJS)
	ar rvcs libbcscgds.a $(OBJS); mv libbcscgds.a $(LIBPATH)

bcscgds : $(OBJFUNCEVAL) 
	$(CC)  $^ $(INCLUDEDIR) $(LIBRARYPATH)  $(CFLAGS) $(LFLAGS) $(MADSFLAG) -o $@ 

$(OBJDIR)/%.o : $(SRC)/%.c 
	$(CC) $^ -c $(INCLUDEDIR) $(CFLAGS) $(LFLAGS) -o $@

$(OBJDIR)/%.o : %.f
	$(CC) -c $^ $(INCLUDEDIR) $(CFLAGS) $(FORTRANFLAG) -o $@

$(OBJDIR)/%.o : %.for
	$(CC) -c $^ $(INCLUDEDIR) $(FORTRANFLAG) $(MADSFLAG) -o $@

cleanFull:
	rm -f  $(OBJS) bcscgds LIBS/libbcscgds.a libbcscgds.a *.o *.a

clean:
	rm -f bcscgds
