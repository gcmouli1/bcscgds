/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "funcConstrSync.h"

double funcEvalBoxConstr (vectorD *point, madsStructure *mads, userData *uData)
{
	int i;
	double output;
	memcpy (mads->tmpPt, point->vec, sizeof(double)*point->dimension);
	bool ptInfeasible=false;
	for (i=0; i<mads->dimension; i++)
	{
		if (point->vec[i] < mads->varLB->vec[i] || point->vec[i] > mads->varUB->vec[i])
		{
			ptInfeasible=true;
		}
	}
	if (ptInfeasible == false)
	{
		// printf ("point sent is feasible for bound constraints\n");
	}
	else
	{
		// printf ("point sent is infeasible for bound constraints\n");
	}


	if (ptInfeasible==false)
	{
		constrEval (mads->tmpPt, mads->constrVec, uData);
		mads->constrValue=0;
		if (mads->objectiveType == 'C')
		{
			output=0;
			for (i=0; i<mads->numConstr; i++)
			{
				//output += pow(mads->constrVec[i],2);
				output += pow(MAX (0,mads->constrVec[i]),2);
			}
		}
		else
		{
			output=funcEvalValue (mads->tmpPt, uData);
			// printf ("func Value is %lf\n",output);

			fprintf(mads->funcList,"%g\n",output);
		}

		if (mads->numConstr == 0)
		{
			mads->constrValue=0;
		}
		else
		{
			for (i=0; i<mads->numConstr; i++)
			{
				mads->exactPenaltyConstr->vec[i]=MAX (0,mads->constrVec[i]);
				mads->constrValue += mads->exactPenaltyConstr->vec[i];
			}
		}
		// printf ("total constr Value is %G\n",mads->constrValue);
		//		output=output+mads->penaltyParam*mads->constrValue;
		mads->ptInfeasible=false;
	}
	else
	{
		// printf ("point is infeasible from funcEvalBoxConstr\n");
		output=DBL_MAX;
		mads->ptInfeasible=true;
	}
	return output;
}

