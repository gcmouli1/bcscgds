/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "quadLinearHybrid.h"
void scgInitQL (scgStructQL *conjGrad, int dimension)
{
	conjGrad->cgIterationMax=1000*(dimension+1);
	conjGrad->cgEpsilon=1E-7;
	conjGrad->oldPt=malloc (sizeof(vectorD));
	vectorInit (conjGrad->oldPt, dimension);
	conjGrad->newPt=malloc (sizeof(vectorD));
	vectorInit (conjGrad->newPt, dimension);
	conjGrad->oldGrad=malloc (sizeof(vectorD));
	vectorInit (conjGrad->oldGrad, dimension);
	conjGrad->newGrad=malloc (sizeof(vectorD));
	vectorInit (conjGrad->newGrad, dimension);
	conjGrad->cgDirection=malloc (sizeof(vectorD));
	vectorInit (conjGrad->cgDirection, dimension);
	conjGrad->vicinityPt=malloc (sizeof(vectorD));
	vectorInit (conjGrad->vicinityPt, dimension);
	conjGrad->diffGrad=malloc (sizeof(vectorD));
	vectorInit (conjGrad->diffGrad, dimension);
	conjGrad->diffPts=malloc (sizeof(vectorD));
	vectorInit (conjGrad->diffPts, dimension);
	conjGrad->thetaMatrix=malloc (sizeof(matrixD));
	matrixInit (conjGrad->thetaMatrix, dimension, dimension);
}

void quadScgMadsQL (madsStructure *mads, scgStructQL *conjGrad, simplexGradStruct *sg, lineSearchStruct *gs, userData *uData)
{
	//	centroidGenerate (mads);
	double optValTmp;
	vectorD *optimum=malloc (sizeof(vectorD));
	vectorInit (optimum, mads->dimension);
	scgInitQL (conjGrad, mads->dimension);
	// printf ("printing mads->radius before poll step %f\n", mads->radius);
	pollStep (mads->center, mads, mads->radius, mads->numPollSteps, uData);
	if (mads->pollStepLimitCrossed == true)
	{
		return;
	}
	if (mads->badDistribution == false)
	{
		quadMatrix(mads);
		quadHessianPositivity (mads, optimum);
		if (mads->quadFitPSD == true)
		{
			if (mads->ptInfeasible == false)
			{
				optValTmp=singlePtEval (optimum,mads, uData);
			}
			else
			{
				vectorAdd (1.0, optimum, -1.0, mads->center, mads->direction);
				vectorNormalize2 (mads->direction);
				//	// printf ("printing mads->direction in CG method \n");
				//printVector (mads->direction);
				brent (gs, mads, mads->center, mads->direction, uData);
			}
			// printf ("Actual value of quadratic min %lf\n",optValTmp);
		}
	}
	if (mads->iterLimitCrossed == true || mads->radius < mads->radiusLimit)
	{
		return;
	}

	double optValue=mads->funcSet->vec[mads->centerIndex];
	if (mads->badDistribution == false)
	{
		simplexGradient (mads, sg);
		negGradFuncEval (mads, uData);
	}
	else
	{
		copyVectors (mads->bestPt,mads->gradient);
		scaleVector (mads->gradient, -1.0);
	}

	copyVectors (mads->gradient, conjGrad->oldGrad);
	if (mads->iterLimitCrossed == true || mads->radius < mads->radiusLimit)
	{
		return;
	}

	if (mads->negGradFuncValue < optValue)
	{
		mads->gradDescent = true;
	}
	else
	{
		mads->gradDescent = false;
	}

	vicinitySearch (mads, uData);
	copyVectors (mads->bestPt, conjGrad->vicinityPt);
	copyVectors (mads->center, conjGrad->oldPt);
	vectorAdd (1.0, mads->bestPt, -1.0, mads->center, conjGrad->cgDirection);
	double preLSbestValue=mads->bestVal;
	brent (gs, mads, mads->center, conjGrad->cgDirection, uData);
	if (mads->iterLimitCrossed == true || mads->radius < mads->radiusLimit)
	{
		return;
	}
	double postLSbestValue=mads->bestVal;
	double diffLSbestValues=preLSbestValue-postLSbestValue;
	// printf ("printing function decrease caused by Brent Algo %lf\n",diffLSbestValues);
	copyVectors (mads->bestPt, conjGrad->newPt);

	vectorAdd (1.0, conjGrad->newPt, -1.0, conjGrad->oldPt, conjGrad->diffPts);
	// printf ("printing vector diffPts\n");
	//printVector (conjGrad->diffPts);
	mads->centralDistance=vectorNorm2 (conjGrad->diffPts);
	// printf ("central distance is %lf while radius is %lf\n",mads->centralDistance, mads->radius);

	double beta, betaTmp, diffGradTdiffPts, thetaTmp, newGradNorm;
	int cgIterations=0;
	bool optCondition=false;
	double radius=mads->radius;

	while (optCondition == false && cgIterations < conjGrad->cgIterationMax)
	{
		copyVectors (conjGrad->newPt, mads->center);
		mads->centerIndex=mads->bestPtIndex;
		if (diffLSbestValues < mads->radiusRebootPrecision)
		{
			//radius = MIN (1, mads->boxRadius);
			radius = MAX (1, mads->boxRadius);
		}
		else
		{
			radius=mads->radius;
		}
		vectorAdd (1.0, conjGrad->newPt, -1.0, conjGrad->oldPt, conjGrad->diffPts);
		//	// printf ("printing vector diffPts\n");
		//	printVector (conjGrad->diffPts);
		mads->centralDistance=vectorNorm2 (conjGrad->diffPts);
		// printf ("central distance is %e while radius is %e\n",mads->centralDistance, mads->radius);

		if (mads->centralDistance > 2*mads->radius)
		{
			mads->radius=4*mads->radius;
		}
		else if (mads->centralDistance > mads->radius)
		{
			mads->radius=mads->centralDistance;
		}
		else
		{
		}

		radius=mads->radius;

	//	 printf ("value of diffLSbestValues is %e\n",diffLSbestValues);
		pollStep (conjGrad->newPt, mads, radius, mads->numPollSteps, uData);
		if (mads->pollStepLimitCrossed == true)
		{
			return;
		}
		if (mads->iterLimitCrossed == true || mads->radius < mads->radiusLimit)
		{
			return;
		}
		if (mads->badDistribution == false)
		{
			quadMatrix (mads);
			quadHessianPositivity (mads, optimum);
			if (mads->quadFitPSD == true)
			{
				if (mads->ptInfeasible == false)
				{
					optValTmp=singlePtEval (optimum, mads, uData);
				}
				else
				{
					vectorAdd (1.0, optimum, -1.0, mads->center, mads->direction);
					vectorNormalize2 (mads->direction);
					brent (gs, mads, mads->center, mads->direction, uData);
				}
				// printf ("Actual value of quadratic min %lf\n",optValTmp);
			}
		}

		optValue=mads->funcSet->vec[mads->centerIndex];
		//	// printf ("printing bestPt before simplex gradient computation\n");
		//	printVector (mads->bestPt);
		if (mads->badDistribution == false)
		{
			simplexGradient (mads, sg);
			negGradFuncEval (mads, uData);
		}
		else
		{
			//	copyVectors (mads->bestPt,mads->gradient);
			//	scaleVector (mads->gradient, -1.0);

			//	copyVectors (mads->bestPt, gs->direction);
			vectorAdd (1.0, mads->bestPt, -1.0, mads->center, gs->direction);
			copyVectors (mads->center, conjGrad->oldPt);
			preLSbestValue=mads->bestVal;
			brent (gs, mads, mads->center, gs->direction, uData);
			postLSbestValue=mads->bestVal;
			diffLSbestValues=preLSbestValue-postLSbestValue;
			copyVectors (mads->bestPt, conjGrad->newPt);

			//	// printf ("printing gs->direction in CG method \n");
			//	printVector (gs->direction);
		}
		//	// printf ("printing gradient at newPt in CG\n");
		//	printVector (mads->gradient);
		if (mads->iterLimitCrossed == true)
		{
			return;
		}
		if (mads->badDistribution == false)
		{
			copyVectors (mads->gradient, conjGrad->newGrad);
			newGradNorm = vectorNorm2 (conjGrad->newGrad);
			if (newGradNorm <= conjGrad->cgEpsilon || mads->radius < mads->radiusLimit)
			{
				optCondition = true;
				// printf ("norm of gradient is very low\n");
				break;
			}
			if (mads->negGradFuncValue < optValue)
			{
				mads->gradDescent = true;
				// printf ("negative of gradient is descent direction \n");
			}
			else
			{
				mads->gradDescent = false;
				// printf ("negative of gradient is not a descent direction \n");
			}
			//	// printf ("printing bestPt before vicinity search\n");
			//	printVector (mads->bestPt);
			vicinitySearch (mads, uData);

			copyVectors (mads->bestPt, conjGrad->vicinityPt);
			if (mads->iterLimitCrossed == true)
			{
				return;
			}

			if (mads->gradDescent == true)
			{
				vectorAdd (1.0, conjGrad->newGrad, -1.0, conjGrad->oldGrad, conjGrad->diffGrad);
				diffGradTdiffPts = vectorDotProd (conjGrad->diffGrad, conjGrad->diffPts);
				scaleMatrix (conjGrad->thetaMatrix, 0);
				matrixRankOneOrig (-1.0, conjGrad->vicinityPt, conjGrad->vicinityPt, conjGrad->thetaMatrix);
				thetaTmp = vectorDotProd (conjGrad->vicinityPt, conjGrad->newGrad);
				scaleMatrix (conjGrad->thetaMatrix, thetaTmp);
				vectorMatrixMult ('N', 1.0, conjGrad->thetaMatrix, conjGrad->diffGrad, -1.0, conjGrad->diffPts, mads->tmpVec);
				betaTmp = vectorDotProd (mads->tmpVec, conjGrad->newGrad);
				beta = betaTmp/diffGradTdiffPts;
				// printf ("value of diffGradTdiffPts is %lf\n",diffGradTdiffPts);
				// printf ("value of betaTmp is %lf\n",betaTmp);
				// printf ("value of beta is %lf\n",beta);
				//vectorMatrixMult ('N', -1.0, mads->thetaMatrix, mads->newGrad, beta, mads->diffPts, mads->cgDirection);
				vectorAdd (1.0, conjGrad->vicinityPt, beta, conjGrad->diffPts, conjGrad->cgDirection);
				copyVectors (conjGrad->newPt, conjGrad->oldPt);
				// printf ("printing CG direction when grad descent is true \n");
				//printVector (conjGrad->cgDirection);
			}
			else
			{
				copyVectors (conjGrad->newPt, conjGrad->oldPt);
				// printf ("printing oldPt \n");
				//printVector (conjGrad->oldPt);
				//// printf ("printing newPt \n");
				//printVector (conjGrad->newPt);
				//		// printf ("printing bestPt \n");
				//		printVector (mads->bestPt);

				vectorAdd (1.0, mads->bestPt, -1.0, conjGrad->oldPt, conjGrad->cgDirection);

				// printf ("printing CG direction when grad descent is false \n");
				//printVector (conjGrad->cgDirection);
			}

			copyVectors (conjGrad->cgDirection, gs->direction);
			// printf ("printing the starting point for line search \n");
			//printVector (conjGrad->newPt);
			preLSbestValue=mads->bestVal;
			//	// printf ("printing gs->direction in CG method \n");
			//	printVector (gs->direction);
			brent (gs, mads, conjGrad->newPt, gs->direction, uData);
			postLSbestValue=mads->bestVal;
			diffLSbestValues=preLSbestValue-postLSbestValue;
			// printf ("printing function decrease caused by Brent Algo %lf\n",diffLSbestValues);
			if (sg->quickSG == true && mads->centralDistance < mads->radius && diffLSbestValues>0)
			{
				copyVectors (conjGrad->newPt, conjGrad->oldPt);
			}
			copyVectors (mads->bestPt, conjGrad->newPt);
			if (mads->iterLimitCrossed == true)
			{
				return;
			}
		}
		cgIterations++;
		// printf ("printing number of CG iterations %d\n",cgIterations);
		if (diffLSbestValues < mads->diffLSbestValuePrecision)
		{
			// printf ("No further improvement \n");
			// printf ("seems like need to restart \n");
			radius=MIN (1,mads->boxRadius);
		}
	}
}


void freeSCGQL (scgStructQL *conjGrad)
{
	freeVectorD (conjGrad->oldPt);
	freeVectorD (conjGrad->newPt);
	freeVectorD (conjGrad->oldGrad);
	freeVectorD (conjGrad->newGrad);
	freeVectorD (conjGrad->cgDirection);
	freeVectorD (conjGrad->vicinityPt);
	freeVectorD (conjGrad->diffGrad);
	freeVectorD (conjGrad->diffPts);
	freeMatrixD (conjGrad->thetaMatrix);
	free (conjGrad);
}
