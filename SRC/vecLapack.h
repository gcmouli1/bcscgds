/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _VECLAPACK_H
#define _VECLAPACK_H
#include "vecBlas.h"

void dlacpy_( char *UPLO, int *rows, int *cols, double *matrix1, int *LDA, double *matrix2, int *LDB);
void dtpttr_( char *UPLO, int *rows, double *inputMat, double *Atemp, int *LDA, int *INFO);
void dgesv_(int *rows, int *NRHS, double *Atemp, int *LDA, int *IPIV, double *outputVec, int *LDB, int *INFO);
void dppsv_(char *UPLO, int *rows, int *NRHS, double *Atemp, double *outputVec, int *LDB, int *INFO);
void dgels_(char *TRANS, int *rows, int *cols, int *NRHS, double *Atemp, int *LDA, double *outputVec, int *LDB, double *WORK, int *LWORK, int *INFO);
double dlange_(char *NORM, int *rows, int *cols, double *A, int *LDA, double *WORK);
void dpptrf_(char *UPLO, int *rows, double *Atemp, int *INFO);
void dpotrf_(char *UPLO, int *rows, double *mat, int *LDA, int *INFO);
void dgeqrf_(int *rows, int *cols, double *Atemp, int *LDA, double *TAU, double *WORK, int *LWORK, int *INFO);
void dormqr_(char *side, char *TRANS, int *rowsC, int *colsC, int *k, double *outputMat, double *TAU, double *C, int *LDC, double *WORK, int *LWORK, int *INFO);
void dgesdd_(char *JOBZ, int *rows, int *cols, double *Atemp, int *LDA, double *S, double *U, int *LDU, double *VT, int *LDVT, double *WORK, int *LWORK, int *IWORK, int *INFO);
void dpotri_(char *UPLO, int *rows, double *mat, int *LDA, int *INFO);
void dlarf_(char *position, int *rows, int *cols, double *vec, int *INCV, double *tau, double *mat, int *LDC, double *WORK);
void dsyevr_(char *JOBZ, char *RANGE, char *UPLO, int *rows, double *mat, int *LdA, double *VL, double *VU, int *IL, int *IU, double *ABSTOL, int *M, double *vec, double *Z, int *LDZ, int *ISUPPZ, double *WORK, int *LWORK, int *IWORK, int *LIWORK, int *INFO);

void copyMatrixLAPACK (char UPLO, matrixD *matrix1,  matrixD *matrix2);
void packedToFullMatrix (char UPLO, matrixD *inputMat, matrixD *outputMat);
//void linearSystem (matrixD *A, vectorD *B, vectorD *outputVec);
void linearSystem (matrixD *A, vectorD *B, vectorD *outputVec, int *info);
void linearSystemSym (char UPLO, matrixD *A, vectorD *B, vectorD *outputVec);
void linearSystemSymOrig (char UPLO, matrixD *A, vectorD *B, vectorD *outputVec);
void leastSquare (char TRANS, matrixD *A, vectorD *B, vectorD *outputVec);
void leastSquareOrig (char TRANS, matrixD *A, vectorD *B);
double matrixNorm (char NORM, matrixD *A);
void matrixCholeskyDec (char UPLO, matrixD *A, matrixD *outputMat);
void matrixCholeskyDecOrig (char UPLO, matrixD *A);
void matrixQRfact (matrixD *A, matrixD *outputMat);
void matrixQRfactOrig (matrixD *A);
void matrixQRfactGetQ (matrixD *A, matrixD *outputMat, matrixD *identityMat);
void matrixSVDD (matrixD *A, matrixD *U, vectorD *S, matrixD *VT);
void matrixSPDInverse (char UPLO, matrixD *inputMat, matrixD *outputMat);
void matrixSPDInverseOrig (char UPLO, matrixD *inputMat);
void houseHolderRot (matrixD *inputMat, vectorD *inputVec, char position, double tau);
void eigenValueCompute (matrixD *inputMat, vectorD *eigenValue);

#endif
