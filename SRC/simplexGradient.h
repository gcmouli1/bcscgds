/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _SIMPLEXGRADIENT_H
#define _SIMPLEXGRADIENT_H

#include "quickSort.h"
#include "RemoveDuplicate.h"
#include "singlePtEval.h"

typedef struct 
{
	bool gradDescent;
	int numVicinityPts;
	bool quickSG;
	vectorD *smCenterDist;
	vectorD *tmpGrad;
	vectorD *outgoingVector;
	vectorZ *qsPollDirectionIndex;
	vectorZ *vicinityPointsIndex;
	vectorD *vecOnesSM;
} simplexGradStruct;

void simplexGradInit (madsStructure *mads, simplexGradStruct *sg, bool quickSG);
void vicinityPoints (madsStructure *mads, char searchParameter, simplexGradStruct *sg);
//void quickSimplexGradient (madsStructure *mads, simplexGradStruct *sg);
void simplexGradient (madsStructure *mads, simplexGradStruct *sg);
void negGradFuncEval (madsStructure *mads, userData *uData);
void freeSimplexGrad (simplexGradStruct *sg);

#endif
