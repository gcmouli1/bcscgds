/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "funcEval.h"
#define INFS DBL_MAX

void userIntialize (userData *uData, char **argv)
{
	uData->dimension=atoi(argv[2]);
	int dimension=uData->dimension;
	FILE *fp;
	fp=fopen(argv[1],"r");
	uData->startPt=malloc (sizeof(double)*uData->dimension);
	int i;
       	for (i=0; i<uData->dimension; i++) 
	{
		fscanf(fp,"%lf",&uData->startPt[i]);
	}
	fclose(fp);
	uData->iterLimit=atoi(argv[3]);
	uData->pollPattern=argv[4];
	uData->numConstr=atoi(argv[5]);
	uData->initPtFeasibility=atoi(argv[6]);
	uData->initPtFeasibility=1;
	uData->fpBound=fopen(argv[7],"r");
	uData->problemNum=atoi(argv[8]);

	double *newStartPt=malloc (sizeof(double)*dimension);
	double fmin;
	double xmax;
	int ierr;
//	tiud11_(&dimension, newStartPt, &fmin, &xmax, &problemNum, &ierr);
	tiud26_(&dimension, &uData->numPartialFunction, newStartPt, &fmin, &xmax, &uData->problemNum, &ierr);
	if (ierr != 0)
	{
		printf ("Incorrect input data. Terminating the program\n");
		exit(10);
	}
	free (newStartPt);
	
}
double funcEvalValue(double *point, userData *uData)
{
	double funcval=0;
//	tffu28_(&dimension, x, &funcval, &uData->problemNum);
	int dimension=uData->dimension;
	double funcvalPartial=0; 
	int i;
	int incx=1;
	double norm1 = dasum_(&dimension, point, &incx);
	int indexNormInf = idamax_(&dimension, point, &incx);
	double normInf = point[indexNormInf];
	double norm2 = dnrm2_(&dimension, point, &incx);
	double phi_0 = 0.9*sin(100*norm1)*cos(100*normInf)+0.1*cos(norm2);
	double phi = phi_0*(4*pow(phi_0,2)-3);
	double epsilon=1E-3;

	for (i=1; i<=uData->numPartialFunction; i++)
	{
		tafu26_(&dimension, &i, point, &funcvalPartial, &uData->problemNum);
		funcval += pow(funcvalPartial,2);
	}
	funcval = (1+epsilon*phi)*funcval;
	return funcval;
}
void constrEval (double *x, double *constrVec, userData *uData)
{
}

void boundsEval (double *varLB, double *varUB, double *constrLB, double *constrUB, userData *uData)
{
	int i;
	for (i=0; i<uData->dimension; i++)
	{
		fscanf(uData->fpBound,"%lf %lf",&varLB[i], &varUB[i]);
	}
	fclose(uData->fpBound);
}
