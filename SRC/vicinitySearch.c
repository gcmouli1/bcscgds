/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "vicinitySearch.h"

void vicinitySearch (madsStructure *mads, userData *uData)
{
	int cols;
	double tmpValue;
	double tmpValue2=INFS;
	// printf ("bestPt function value inside vicinitySearch function %e\n",mads->bestVal);
	//sort k best points from searchMatrix
//	// printf ("Printing funcIndex of simplex\n");
//	for (cols=0; cols<mads->searchMatrix->cols; cols++)
//	{
//		// printf("%d\n",mads->funcIndex[cols]);
//	}


	double maxFuncVal=-INFS;
	for (cols=0; cols<mads->numFeasibleElem; cols++)
	{
		if(maxFuncVal<fabs(mads->funcSet->vec[cols]))
		{
			maxFuncVal=fabs(mads->funcSet->vec[cols]);
		}
	}


	int numInfeasible=0;
	for (cols=0; cols<mads->searchMatrix->cols; cols++)
	{
		if (mads->funcIndex[cols] != 2*mads->iterLimit)
		{
			mads->simplexFuncValues->vec[cols]=mads->funcSet->vec[mads->funcIndex[cols]];
		}
		else
		{
			numInfeasible++;
			//mads->simplexFuncValues->vec[cols]=100*mads->funcSet->vec[0];
			mads->simplexFuncValues->vec[cols]=maxFuncVal*100;
		}
	}
//	// printf ("printing simplexFuncValues\n");
//	printVector(mads->simplexFuncValues);

	//int *funcIndexTmp=malloc (sizeof(int)*mads->searchMatrix->cols);
	//for (cols=0; cols<mads->searchMatrix->cols; cols++)
	//{
	//	funcIndexTmp[cols]=cols;
	//}
	quickSort(mads->simplexFuncValues->vec, 0, mads->searchMatrix->cols-1, mads->funcIndex);
	//quickSort(mads->simplexFuncValues->vec, 0, mads->searchMatrix->cols-1, funcIndexTmp);
//	// printf ("Printing sorted funcIndex and funcValue of simplex\n");
//	for (cols=0; cols<mads->searchMatrix->cols; cols++)
//	{
	//	// printf("%d \t %f\n",funcIndexTmp[cols], mads->simplexFuncValues->vec[cols]);
//		// printf("%d \t %f\n",mads->funcIndex[cols], mads->simplexFuncValues->vec[cols]);
//	}
	
	mads->numVicinityPts=MIN(mads->numVicinityPtsMax, mads->searchMatrix->cols-numInfeasible);
	// printf ("numVicinityPts in vicinity search %d\n",mads->numVicinityPts);

	vectorD *tmpVec=malloc (sizeof(vectorD));
	vectorInit (tmpVec, mads->dimension);
	for (cols=0; cols<mads->numVicinityPts; cols++)
	{
		vecExtractMat (tmpVec, mads->ptSet, mads->funcIndex[cols]);
		//vectorAddOrig (-0.5, mads->gradient, 0.5, tmpVec); //negative for gradient since it is descent direction
//		// printf("printing extracted vector in vicinity search\n");
//		printVector (tmpVec);
		vectorAddOrig (0.5, mads->bestPt, 0.5, tmpVec); 
		vectorAddOrig (-1.0, mads->center, 1.0, tmpVec);
		vectorNormalize2 (tmpVec);
		scaleVector (tmpVec, mads->radius);
		vectorAddOrig (1.0, mads->center, 1.0, tmpVec);
		tmpValue=singlePtEval (tmpVec, mads, uData);
		// printf("printing function value of average point in vicinity search %e \n", tmpValue);
		if (tmpValue2 > tmpValue)
		{
			tmpValue2 = tmpValue;
		}
		if (mads->iterLimitCrossed == true)
		{
			return;
		}
	}
	//free(funcIndexTmp);
	if (tmpValue2 > mads->bestVal)
	{
		// printf ("Vicinity search successful\n");
	}
	freeVectorD (tmpVec);
}
