/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "vecMatDeclare.h"
void vectorInitZ (vectorZ *inputVec, int numElem)
{
	inputVec->dimension=numElem;
	inputVec->vec=malloc (sizeof(int)*numElem);
}

void vectorInit(vectorD *inputVec, int numElem)
{
	inputVec->dimension=numElem;
	inputVec->vec=malloc (sizeof(double)*numElem);
}

void matrixInit(matrixD *inputMat, int rows, int cols)
{
	inputMat->rows=rows;
	inputMat->cols=cols;
	inputMat->mat=malloc (sizeof(double)*rows*cols);
}

void matrixInitFromMatrix(matrixD *inputMat, matrixD *A)
{
	inputMat->rows=A->rows;
	inputMat->cols=A->cols;
	inputMat->mat=malloc (sizeof(double)*(inputMat->rows)*(inputMat->cols));
}

void vectorMax(vectorD *inputVec, double *maxVal, int *maxIndex)
{
	int index;
	*maxVal=-1*INFS;
	for(index=0; index<inputVec->dimension; index++)
	{
		if(*maxVal<inputVec->vec[index])
		{
			*maxVal=inputVec->vec[index];
			*maxIndex=index;
		}
	}
}

void vectorMin(vectorD *inputVec, double *minVal, int *minIndex)
{
	int index;
	*minVal=INFS;
	for(index=0; index<inputVec->dimension; index++)
	{
		if(*minVal>inputVec->vec[index])
		{
			*minVal=inputVec->vec[index];
			*minIndex=index;
		}
	}
}

void matrixMax(matrixD *inputMat, double *maxVal, int *maxIndex, int *rowNum, int *colNum)
{
	int index;
	*maxVal=-INFS;
	int numElem=(inputMat->rows)*(inputMat->cols);
	for(index=0; index<numElem; index++)
	{
		if(*maxVal<inputMat->mat[index])
		{
			*maxVal=inputMat->mat[index];
			*maxIndex=index;
		}
	}
	*colNum=index/(inputMat->rows);
	*rowNum=index-(*colNum)*(inputMat->rows);
}

void matrixMin(matrixD *inputMat, double *minVal, int *minIndex, int *rowNum, int *colNum)
{
	int index;
	*minVal=INFS;
	int numElem=(inputMat->rows)*(inputMat->cols);
	for(index=0; index<numElem; index++)
	{
		if(*minVal>inputMat->mat[index])
		{
			*minVal=inputMat->mat[index];
			*minIndex=index;
		}
	}
	*colNum=index/(inputMat->rows);
	*rowNum=index-(*colNum)*(inputMat->rows);
}

void printVectorZ (vectorZ *vectorA)
{
	int i;
	for (i=0; i<vectorA->dimension; i++)
	{
		printf ("%d\n", vectorA->vec[i]);
	}
}

void printVector (vectorD *vectorA)
{
	int i;
	for (i=0; i<vectorA->dimension; i++)
	{
		printf ("%G\n", vectorA->vec[i]);
	}
}

void printVectorL (vectorD *vectorA, int numElem)
{
	int i;
	if (numElem > vectorA->dimension)
	{
		printf ("given size is beyond the vector dimension\n");
	}
	else
	{
		for (i=0; i<numElem; i++)
		{
			printf ("%G\n", vectorA->vec[i]);
		}
	}
}
void printMatrix (matrixD *matrixA)
{
	int i, j;
	int rows=matrixA->rows;
	int cols=matrixA->cols;
	for (i=0; i<rows; i++)
	{
		for (j=0; j<cols; j++)
		{
			printf ("%G ", matrixA->mat[i+j*rows]);
		}
		printf ("\n");
	}
}

void printMatrixL (matrixD *matrixA, int rows, int cols)
{
	int i, j;
	//int rows=matrixA->rows;
	//int cols=matrixA->cols;
	if (rows > matrixA->rows || cols > matrixA->cols )
	{
		printf ("given dimensions are beyond the dimension of matrix\n");
	}
	else
	{
		for (i=0; i<rows; i++)
		{
			for (j=0; j<cols; j++)
			{
				printf ("%G ", matrixA->mat[i+j*rows]);
			}
			printf ("\n");
		}
	}
}

void vecOnesZ (vectorZ *vector, const int dimension)
{
	vector->dimension=dimension;
	vector->vec=malloc (sizeof (int)*dimension);
	int i;
	for (i=0; i<vector->dimension; i++)
	{
		vector->vec[i]=1;
	}
}

void vecOnesD (vectorD *vector, const int dimension)
{
	vector->dimension=dimension;
	vector->vec=malloc (sizeof (double)*dimension);
	int i;
	for (i=0; i<vector->dimension; i++)
	{
		vector->vec[i]=1;
	}
}

void vecZerosZ (vectorZ *vector, const int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (int));
}

void vecZerosD (vectorD *vector, const int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (double));
}

void vecEyeD (vectorD *vector, const int elemNum, const int dimension)
{
	vector->dimension=dimension;
	vector->vec=calloc (vector->dimension, sizeof (double));
	vector->vec[elemNum]=1.0;
}

void matEyeD (matrixD *matrix, const int rows, const int cols)
{
	matrix->rows=rows;
	matrix->cols=cols;
	matrix->mat=calloc ((matrix->rows)*(matrix->cols), sizeof (double));
	int i;
	for (i=0; i<matrix->rows; i++)
	{
		matrix->mat[(matrix->rows+1)*i]=1.0;
	}
}

void matOnesD (matrixD *matrix, const int rows, const int cols)
{
	matrix->rows=rows;
	matrix->cols=cols;
	matrix->mat=malloc (sizeof (double)*(matrix->rows*matrix->cols));
	int i=0;
	for (i=0; i<rows*cols; i++)
	{
		matrix->mat[i]=1.0;
	}
}

void diagMatrix (vectorD *inputVec, matrixD *outputMat)
{
	int dimension=inputVec->dimension;
	int i=0;
	for (i=0; i<dimension; i++)
	{
		outputMat->mat[(outputMat->rows+1)*i]=inputVec->vec[i];
	}
}

void diagMatrixInverse (vectorD *inputVec, matrixD *outputMat)
{
	int dimension=inputVec->dimension;
	int i=0;
	for (i=0; i<dimension; i++)
	{
		outputMat->mat[(outputMat->rows+1)*i]=1.0/inputVec->vec[i];
	}
}

void matZerosD (matrixD *matrix, const int rows, const int cols)
{
	matrix->rows=rows;
	matrix->cols=cols;
	matrix->mat=calloc ( (matrix->rows)*(matrix->cols), sizeof (double));
}

double elemExtractVecD (const vectorD *vector, const int elemNum)
{
	return vector->vec[elemNum];
}

double elemExtractMatD (const matrixD *matrix, const int x, const int y)
{
	return matrix->mat[x+y*matrix->rows];
}

void matrixTranspose (matrixD *inputMat, matrixD *outputMat)
{
	int i, j;
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	for (i=0; i<rows; i++)
	{
		for (j=0; j<cols; j++)
		{
			outputMat->mat[j+i*cols]=inputMat->mat[i+j*rows];
		}
	}
	//return outputMat;
}

void upperLowerFull (char destination, matrixD *inputMat)
{
	int rows=inputMat->rows;
	int i, j;
	if (destination=='U') /*Copy from lower to upper */
	{
		for (i=0; i<rows-1; i++)
		{
			for (j=i+1; j<rows; j++)
			{
				inputMat->mat[i+j*rows]=inputMat->mat[j+i*rows];
			}
		}
	}
	else if (destination=='L') /*Copy from upper to lower */
	{
		for (i=0; i<rows-1; i++)
		{
			for (j=i+1; j<rows; j++)
			{
				inputMat->mat[j+i*rows]=inputMat->mat[i+j*rows];
			}
		}
	}
	else
	{
		printf ("Wrong input for matrix type \n");
		exit (10);
	}

}

void vecExtractMat (vectorD *vector, matrixD *matrix, int colnum)
{
	int rows=matrix->rows;
	memcpy (vector->vec, matrix->mat+(rows*colnum), rows * sizeof(double));
}

void mat2Dextract (matrixD *matrix, vectorD **vectorCollectStruct)
{
	int rows=matrix->rows;
	int cols;
	for (cols=0; cols<matrix->cols; cols++)
	{
		memcpy (vectorCollectStruct[cols]->vec, matrix->mat+(rows*cols), rows * sizeof(double));
	}
}

void submatrixExtract (matrixD *inputMat, int *colIndex, int numColElem, matrixD *outputMat)
{
	int rows=inputMat->rows;
	//	matrixD *outMat=malloc (sizeof (matrixD));
	//	outMat->rows=inputMat->rows;
	//	outMat->cols=colElem;
	//	outMat->mat=malloc (sizeof (double)*rows*colElem);
	int cols;
	for (cols=0; cols<numColElem; cols++)
	{
		memcpy (outputMat->mat+rows*cols, inputMat->mat+(rows*colIndex[cols]), rows*sizeof (double));
	}
	//	return outputMat;
}

void vectorVectorAppend (vectorD *vec1, vectorD *vec2, vectorD *outputVec)
{
	memcpy (outputVec->vec, vec1->vec, sizeof(double)*(vec1->dimension));
	memcpy (outputVec->vec+vec1->dimension, vec2->vec, sizeof(double)*(vec2->dimension));
}

void matrixVectorAppend (char position, matrixD *inputMat, vectorD *inputVec)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	if (rows != inputVec->dimension)
	{
		printf ("matrix and vector are not of compatible sizes\n");
		exit (10);
	}
	double *tmp;
	if (position=='F')
	{
		tmp=malloc (sizeof (double)*rows*(cols+1));
		memcpy (tmp, inputVec->vec, rows*sizeof (double));
		memcpy (tmp+inputVec->dimension, inputMat->mat, sizeof(double)*rows*cols);
		free (inputMat->mat);
	}
	else
	{
		tmp=realloc (inputMat->mat, sizeof(double)*rows*(cols+1));
		memcpy (tmp+rows*cols, inputVec->vec, rows*sizeof(double));
		if (tmp == NULL)
		{
			printf ("realloc returning NULL pointer. May be not enough memory case\n");
			exit (10);
		}

	}
	inputMat->mat=tmp;
	inputMat->cols=cols+1;
}

void matrixMatrixHorizAppend (matrixD *matrix1, matrixD *matrix2)
{
	int rows1=matrix1->rows;
	int cols1=matrix1->cols;
	int rows2=matrix2->rows;
	int cols2=matrix2->cols;
	double *tmp;
	if (rows1 != rows2)
	{
		printf ("Horizontal append. Matrices are not of compatible sizes \n");
		exit (10);
	}
	tmp=realloc (matrix1->mat, sizeof (double)*( (rows1+rows2)*(cols1+cols2)));
	if (tmp == NULL)
	{
		printf ("realloc returning NULL pointer. May be not enough memory case\n");
		exit (10);
	}
	matrix1->mat=tmp;
	memcpy (matrix1->mat+rows1*cols1, matrix2->mat, sizeof (double)*rows2*cols2);
	matrix1->cols=cols1+cols2;
}

void matrixVectorVertAppend (char position, matrixD *inputMat, vectorD *inputVec, matrixD *outputMat)
{
	int rows=inputMat->rows;
	int cols=inputMat->cols;
	if (cols != inputVec->dimension)
	{
		printf ("matrix and vector are not of compatible sizes \n");
		exit (10);
	}
	int i;
	if (position=='T')
	{
		for (i=0; i<cols; i++)
		{
			memcpy (outputMat->mat+(rows+1)*i, inputVec->vec+i, sizeof (double)*1);
			memcpy (outputMat->mat+(rows+1)*i+1, inputMat->mat+(rows*i), sizeof (double)*rows);
		}
	}
	if (position=='B')
	{
		for (i=0; i<cols; i++)
		{
			memcpy (outputMat->mat+(rows+1)*i, inputMat->mat+(rows*i), sizeof (double)*rows);
			memcpy (outputMat->mat+(rows+1)*(i+1)-1, inputVec->vec+i, sizeof (double)*1);
		}
	}
	//return outputMat;
}

void matrixMatrixVertAppend (matrixD *matrix1, matrixD *matrix2, matrixD *outputMat)
{
	int rows1=matrix1->rows;
	int cols1=matrix1->cols;
	int rows2=matrix2->rows;
	int cols2=matrix2->cols;
	if (cols1 != cols2)
	{
		printf ("Matrix Matrix vertical append. Matrices are not of compatible sizes \n");
		exit (10);
	}

	//matrixD *outputMat=malloc (sizeof (matrixD));
	//outputMat->rows=rows1+rows2;
	//outputMat->cols=cols1;
	//outputMat->mat=malloc (sizeof (double)*(rows1+rows2)*(cols1+cols2));
	int i;
	for (i=0; i<cols1; i++)
	{
		memcpy (outputMat->mat+(rows1+rows2)*i, matrix1->mat+(rows1*i), sizeof (double)*rows1);
		memcpy (outputMat->mat+(rows1+rows2)*i+rows1, matrix2->mat+(rows2*i), sizeof (double)*rows2);
	}
	//return outputMat;
}

void vectorMatrixConvert(char type, vectorD *vectorA, matrixD *matrixA)
{
	if (type=='V')
	{
		vectorA->dimension=matrixA->rows * matrixA->cols;
		vectorA->vec=malloc (sizeof(double)*vectorA->dimension);
		memcpy (vectorA->vec, matrixA->mat, sizeof(double)*(vectorA->dimension));
	}
	if (type=='M')
	{
		matrixA->rows=vectorA->dimension;
		matrixA->cols=1;
		matrixA->mat=malloc (sizeof(double)*vectorA->dimension);
		memcpy (matrixA->mat, vectorA->vec, sizeof(double)*(vectorA->dimension));
	}
}


void freeVectorD (vectorD *vector)
{
	//	// printf ("Free the vector structure\n");
	free (vector->vec);
	free (vector);
}

void freeVectorZ (vectorZ *vector)
{
	//	// printf ("Free the vector structure\n");
	free (vector->vec);
	free (vector);
}

void freeMatrixD (matrixD *matrix)
{
	//	// printf ("Free the matrix structure\n");
	free (matrix->mat);
	free (matrix);
}
