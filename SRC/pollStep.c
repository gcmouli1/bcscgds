/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "pollStep.h"

void pollStep (vectorD *point, madsStructure *mads, double radius, int maxPollSteps, userData *uData)
{
//	// printf ("printing point in pollStep beginning\n");
//	printVector (point);
//	// printf ("printing best point in pollStep beginning\n");
//	printVector (mads->bestPt);
	// printf ("printing mads radius before poll step %G\n",mads->radius);
	// printf ("updating poll step mads radius to avoid violation of box constraints\n");
	int i;
	double radiusTmp=DBL_MAX;
	double diffRadius;
	for (i=0; i<mads->dimension; i++)
	{
		diffRadius=point->vec[i]-mads->varLB->vec[i];
		if (diffRadius < radiusTmp)
		{
			radiusTmp=diffRadius;
		}
		diffRadius=mads->varUB->vec[i]-point->vec[i];
		if (diffRadius < radiusTmp)
		{
			radiusTmp=diffRadius;
		}
	}
	if (radiusTmp > pow(mads->madsRecFactor1,4)*mads->radiusLimit)
	{
		mads->radius = MIN (mads->radius, radiusTmp);
		radius = MIN (radius, radiusTmp);
		// printf ("printing mads->radius=%G and sent radius=%G\n",mads->radius,radius);
	}
	radiusTmp=radius;

	mads->allbadPts = true;
	mads->allWorsePts=true;
	int dimension=point->dimension;
	int cols=0;
	int colsActual=0;
	int k;
	int sMCols=mads->searchMatrix->cols;
	int numSearchMatrixElem=mads->searchMatrix->rows*mads->searchMatrix->cols;
	memcpy (mads->ptsInterpolate->mat, point->vec, sizeof(double)*(point->dimension));

	mads->ptIndex=-1;
	mads->interpolateFuncVal->vec[0]=singlePtEval (point, mads, uData);
	// printf ("printing point index %d and bestPtIndex %d\n",mads->ptIndex,mads->bestPtIndex);
	if (mads->ptIndex == -1)
	{
		// printf ("some issue with computing index of center\n");
		exit (10);
	}
//	vecExtractMat (mads->tmpVec, mads->ptSet, mads->ptIndex);
//	// printf ("printing current center at ptIndex=%d\n",mads->ptIndex);
//	printVector (mads->tmpVec);
//	vecExtractMat (mads->tmpVec, mads->ptSet, mads->bestPtIndex);
//	// printf ("printing current best point after ptIndex=%d\n",mads->bestPtIndex);
//	printVector (mads->tmpVec);

	int numPollSteps;
	char ptFeasibilityStatus = mads->ptSetSoftFeasibility[mads->ptIndex];
	// printf ("point feasibility status %c\n",ptFeasibilityStatus);
	if (ptFeasibilityStatus == 'S')
	{
		//numPollSteps=3;
		numPollSteps=maxPollSteps;
	}
	else
	{
		numPollSteps=maxPollSteps;
	}

	mads->numIterpolatePts = 1;

	do
	{
		vecExtractMat (mads->tmpVec, mads->centroidCollect, cols);
		vecExtractMat (mads->rotatePt, mads->normSpanVec, 0);
		vectorAddOrig (1.0, mads->tmpVec, -1.0, mads->rotatePt);
		vectorNormalize2 (mads->rotatePt);
		copyMatrix (mads->normSpanVec, mads->searchMatrixOrigin);
		houseHolderRot (mads->searchMatrixOrigin, mads->rotatePt, 'L', 2.0);
		matrixNormalize2 (mads->searchMatrixOrigin);
		copyMatrix (mads->searchMatrixOrigin, mads->searchMatrix);
		scaleMatrix (mads->searchMatrix, radius);

//		double meshLength=mads->madsRadius/mads->meshLenRedFactor;
//		roundmat (mads->searchMatrix, meshLength);

		matrixRankOneOrig (1.0, point, mads->vecOnesSM, mads->searchMatrix);

		int initNumFeasElem=mads->numFeasibleElem;
		rmDuplicate (mads->searchMatrix, mads->funcIndex, mads, uData);
	//	int iter=0;

		int numPollFeasiblePts=mads->numFeasibleElem-initNumFeasElem;
		if (numPollFeasiblePts < dimension+1)
		{
			mads->badDistribution=true;
		}
		else
		{
			mads->badDistribution=false;
		}
	//	double funcValue=INFS;
		// printf("Value of mads->allWorsePts=%d\n",mads->allWorsePts);

		memcpy (mads->ptsInterpolate->mat+dimension+cols*numSearchMatrixElem, mads->searchMatrix->mat, sizeof(double)*numSearchMatrixElem);
		for (k=0; k<sMCols; k++)
		{
			mads->funcEvalPtsTmp->vec[k]=mads->funcSet->vec[mads->funcIndex[k]];
		}
		memcpy (mads->interpolateFuncVal->vec+1+cols*sMCols, mads->funcEvalPtsTmp->vec, sizeof(double)*sMCols);
		mads->numIterpolatePts += numPollFeasiblePts;
		// printf ("value of allbadPts=%d\n",mads->allbadPts);
		if (mads->iterLimitCrossed)
		{
			return;
		}

		if (mads->allWorsePts == true)
		{
			// printf ("time to shrink \n");
			mads->radius=(mads->radius)/mads->madsRecFactor1;
			radius=radius/mads->madsRecFactor1;
			// printf ("printing madsRadius after reduction %e\n", mads->radius);
		}

	//	if (mads->allbadPts == true)
	//	{
	//		// printf ("time to shrink \n");
	//		mads->radius=(mads->radius)/mads->madsRecFactor2;
	//		radius=radius/mads->madsRecFactor2;
	//		// printf ("printing madsRadius after reduction %e\n", mads->radius);
	//	}
	//	vecExtractMat (mads->tmpVec, mads->ptSet, mads->bestPtIndex);
	//	// printf ("printing best point at bestPtIndex=%d inside pollstep\n",mads->bestPtIndex);
	//	printVector (mads->tmpVec);

		// printf ("printing mads->bestPt before penalty related stuff in poll step\n");
	//	printVector (mads->bestPt);
		cols++;
		colsActual=cols;
		// printf ("printing number of poll step calls %d\n",cols);
		int penaltyIter=cols%3;
		if (mads->numConstr != 0)
		{
		if (mads->allWorsePts == true && mads->objectiveType == 'O')
		{
			if (penaltyIter == 0 || mads->radius < mads->radiusLimit)
			{
				if (ptFeasibilityStatus == 'S')
				{
					mads->radius=radiusTmp;
					radius=mads->radius;
				}
				// printf ("Updating penalty term\n");
				mads->penaltyParam=mads->penaltyParam*10;
				int ptIndex = mads->bestPtIndex;
				penaltyUpdate (mads);
				// printf ("printing ptIndex=%d and bestPtIndex=%d\n",ptIndex,mads->bestPtIndex);
				if (mads->bestPtIndex == ptIndex)
				{
					// printf ("continue poll step \n");
				}
				else
				{
					// printf ("better point found with change in penalty\n");

					// printf ("printing best point after penalty update \n");
				//	printVector (mads->bestPt);
					colsActual=cols;
					cols=numPollSteps; //To break the loop
				}
			}
		}
		}
	}
	while (mads->allWorsePts == true && mads->radius > mads->radiusLimit && cols<numPollSteps);
//	while (mads->allbadPts == true && mads->radius > mads->radiusLimit && cols<numPollSteps);
	//while (mads->allWorsePts == true && mads->radius > mads->radiusLimit && cols<numPollSteps);
	// printf("no. of times pollstep carried out %d\n",colsActual);
	if (colsActual == numPollSteps)
	{
		// printf ("no improvement even after %d poll steps. Terminating the program and returning the best point\n",colsActual);
		mads->pollStepLimitCrossed=true;
		return;
	}

	// printf (" printing mads radius after poll step %f\n",mads->radius);
	//	// printf("printing iterpolation matrix\n");
	//	printMatrix (mads->ptsInterpolate);
	// printf ("no. of points in ptsInterpolate %d\n",mads->numIterpolatePts);
}
