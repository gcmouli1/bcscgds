/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "bracketing.h"

void projectBox (vectorD *startPt, vectorD *endPt, vectorD *direction, double *alpha, madsStructure *mads)
{
	double tmp;
	mads->ptInfeasible=false;
//	// printf ("printing stuff inside project Box\n");
//	// printf ("printing endPt\n");
//	printVector (endPt);
//	// printf ("printing start point\n");
//	printVector (startPt);
	//		// printf ("printing lb vector\n");
	//		printVector (mads->varLB);
	//		// printf ("printing ub vector\n");
	//		printVector (mads->varUB);
//	// printf ("printing direction\n");
//	printVector (direction);
	int i=0;
	while (mads->ptInfeasible == false  &&  i < mads->dimension)
	{
		if (endPt->vec[i] < mads->varLB->vec[i] || endPt->vec[i] > mads->varUB->vec[i])
		{
//			// printf ("value of i is %d\n",i);
			mads->ptInfeasible=true;
		}
		i++;
	}

//	// printf ("printing alpha %f\n",*alpha);
	if (mads->ptInfeasible==true)
	{
//		// printf ("given point is infeasible. Projecting it back on box\n");
		double lambda;
		double lambdaLB = -1*DBL_MAX;
		double lambdaUB = DBL_MAX;

		for (i=0; i < mads->dimension; i++)
		{
			if (direction->vec[i] != 0 )
			{
				//				// printf ("lowerbound tmp %f\n",tmp);
				if (direction->vec[i] > 0)
				{
					tmp=(mads->varLB->vec[i]-startPt->vec[i])/direction->vec[i];
					if (lambdaLB < tmp)
					{
						lambdaLB=tmp;
					}
				}
				if (direction->vec[i] < 0) 
				{
					tmp=(mads->varLB->vec[i]-startPt->vec[i])/direction->vec[i];
					if (lambdaUB > tmp)
					{
						lambdaUB=tmp;
					}
				}
			}
		}

		for (i=0; i < mads->dimension; i++)
		{
			if (direction->vec[i] != 0 )
			{
				//				// printf ("lowerbound tmp %f\n",tmp);
				if (direction->vec[i] < 0)
				{
					tmp=(mads->varUB->vec[i]-startPt->vec[i])/direction->vec[i];
					if (lambdaLB < tmp)
					{
						lambdaLB=tmp;
					}
				}
				if (direction->vec[i] > 0) 
				{
					tmp=(mads->varUB->vec[i]-startPt->vec[i])/direction->vec[i];
					if (lambdaUB > tmp)
					{
						lambdaUB=tmp;
					}
				}
			}
		}
//		// printf ("value of lambdaLB=%G, and lambdaUB=%G\n",lambdaLB,lambdaUB);
		double epsilon=1E-8;
		lambda =  lambdaUB-2*epsilon;
		if (lambdaUB < 0)
		{
//			// printf ("seriour error in box projection\n");
		}

		vectorAdd (1.0, startPt, lambda, direction, endPt);
//		// printf ("printing projected vector\n");
	//	printVector (endPt);
		*alpha=lambda;
	}
	bool infeasible=false;
	//	mads->ptInfeasible=false;
	i=0;
	double epsilon=1E-15;
	while (infeasible==false && i < mads->dimension)
	{
		if (endPt->vec[i] < mads->varLB->vec[i]-epsilon || endPt->vec[i] > mads->varUB->vec[i]+epsilon)
		{
			infeasible=true;
		}
		i++;
	}
	if (infeasible==true)
	{
		printf("some error in projection\n");
		exit (10);
	}
}

void bracket (lineSearchStruct *gs, madsStructure *mads, userData *uData) 
{
//	// printf ("Beginning the bracket method \n");
	gs->numItersBracket=0;
	//double quadMinAlpha=1E10;
	//double fquadMinAlpha=1E10;
	double quadMinAlpha=DBL_MAX;
	double fquadMinAlpha=DBL_MAX;
	double gratio=0.3819660;
	gs->startAlpha=0;
	//gs->endAlpha=gs->stepLength;
	gs->endAlpha=mads->radius;
	//	// printf ("printing gs->direction in bracketing at beginning \n");
	//	printVector (gs->direction);
	copyVectors (gs->startPt, gs->startOrig);
	vectorAdd (1.0, gs->startOrig, gs->endAlpha, gs->direction, gs->endPt);
	projectBox (gs->startOrig, gs->endPt, gs->direction, &gs->endAlpha, mads);
	//	// printf ("printing startPt and endPt in bracketing after projectBox \n");
	//	printVector (gs->startPt);
	//	printVector (gs->endPt);
	gs->fstartPt=singlePtEval (gs->startPt, mads, uData);
	gs->fendPt=singlePtEval (gs->endPt, mads, uData);
//	// printf ("printing fstartPt=%lf and fendPt=%lf\n",gs->fstartPt,gs->fendPt);
	if (mads->ptInfeasible==true)
	{
		gs->midAlpha=(gs->endAlpha-gs->startAlpha)*gratio+gs->startAlpha;
		vectorAdd (1.0, gs->startOrig, gs->midAlpha, gs->direction, gs->midPt);
		gs->fmidPt=singlePtEval(gs->midPt, mads, uData);
		return;
	}


	if (gs->stepLength*0.5 < gs->epsilonBracket)
	{
		if (fabs(gs->fendPt-gs->fstartPt) < gs->epsilonBracket)
		{
			gs->midPtLength=gs->stepLength/2.0;
			// printf("no need for bracketing. Almost flat surface or optimum\n");
			// printf ("1. values of gs->fstartPt=%f, gs->fmidPt=%f and gs->fendPt=%f while stepLength=%f\n", gs->fstartPt, gs->fmidPt, gs->fendPt, gs->endAlpha-gs->startAlpha);
			return;
		}
	}

	if (gs->fendPt >= gs->fstartPt)
	{
		gs->midAlpha=(gs->endAlpha-gs->startAlpha)*gratio+gs->startAlpha;
		vectorAdd (1.0, gs->startOrig, gs->midAlpha, gs->direction, gs->midPt);
		gs->fmidPt=singlePtEval(gs->midPt, mads, uData);
	}
	else
	{
		gs->midAlpha=gs->endAlpha;
		gs->fmidPt=gs->fendPt;
		gs->endAlpha=(gs->endAlpha-gs->startAlpha)/gratio+gs->startAlpha;
		vectorAdd (1.0, gs->startOrig, gs->endAlpha, gs->direction, gs->endPt);
		projectBox (gs->startOrig, gs->endPt, gs->direction, &gs->endAlpha, mads);
		gs->fendPt=singlePtEval (gs->endPt, mads, uData);
		// printf ("printing startPt, midPt and endPt\n");
		//printVector (gs->startPt);
		//printVector (gs->midPt);
		//printVector (gs->endPt);
		if (mads->ptInfeasible==true)
		{
			// printf("hello infeasible\n");
			return;
		}
	}

	do
	{
		if (gs->fmidPt < gs->fstartPt)
		{
			if (gs->fmidPt < gs->fendPt)
			{
				gs->midPtLength=gs->midAlpha-gs->startAlpha;
				gs->stepLength=gs->endAlpha-gs->startAlpha;
				// printf ("2. values of gs->fstartPt=%f, gs->fmidPt=%f and gs->fendPt=%f while stepLength=%f\n",gs->fstartPt,gs->fmidPt,gs->fendPt,gs->endAlpha-gs->startAlpha);
				return;
			}

			else if (gs->fmidPt > gs->fendPt)
			{
				/* Checking whether the points are lying on a straight line or not*/
				if (fabs((gs->fmidPt-gs->fstartPt)*(gs->endAlpha-gs->startAlpha)-(gs->fendPt-gs->fstartPt)*(gs->midAlpha-gs->endAlpha)) <= gs->epsilonParabolic)
				{
					// printf ("all points almost on a straight line\n");
					gs->startAlpha=gs->midAlpha;
					gs->fstartPt=gs->fmidPt;
					gs->midAlpha=gs->endAlpha;
					gs->fmidPt=gs->fendPt;
					gs->endAlpha=(gs->midAlpha-gs->startAlpha)/gratio+gs->startAlpha;
					vectorAdd (1.0, gs->startOrig, gs->endAlpha, gs->direction, gs->endPt);
					projectBox (gs->startOrig, gs->endPt, gs->direction, &gs->endAlpha, mads);
					gs->fendPt=singlePtEval (gs->endPt, mads, uData);
					gs->numItersBracket++;
				}
				/* Fit a quadratic over these points */
				else
				{
					// printf ("attempt to fit quadratic over the points \n");
					gs->alphaMatrix->mat[0]=pow(gs->startAlpha,2);
					gs->alphaMatrix->mat[1]=pow(gs->midAlpha,2);
					gs->alphaMatrix->mat[2]=pow(gs->endAlpha,2);
					gs->alphaMatrix->mat[3]=gs->startAlpha;
					gs->alphaMatrix->mat[4]=gs->midAlpha;
					gs->alphaMatrix->mat[5]=gs->endAlpha;
					gs->alphaMatrix->mat[6]=1;
					gs->alphaMatrix->mat[7]=1;
					gs->alphaMatrix->mat[8]=1;

					gs->funcVector->vec[0]=gs->fstartPt;
					gs->funcVector->vec[1]=gs->fmidPt;
					gs->funcVector->vec[2]=gs->fendPt;
					int info;
					linearSystem (gs->alphaMatrix, gs->funcVector, gs->quadFitCoeff, &info);
					if (info !=0)
					{
						exit(10);
					}
					quadMinAlpha=-gs->quadFitCoeff->vec[1]/(2*gs->quadFitCoeff->vec[0]);
					vectorAdd (1.0, gs->startOrig, quadMinAlpha, gs->direction, gs->quadMinAlphaPt);
					projectBox (gs->startOrig, gs->quadMinAlphaPt, gs->direction, &quadMinAlpha, mads);
					fquadMinAlpha=singlePtEval (gs->quadMinAlphaPt, mads, uData);
					if (fquadMinAlpha < gs->fendPt)
					{
						if ( fabs (quadMinAlpha-gs->startAlpha) > fabs (gs->endAlpha-gs->startAlpha))
						{
							gs->startAlpha=gs->endAlpha;
							gs->fstartPt=gs->fendPt;
							copyVectors (gs->endPt, gs->startPt);
							gs->midAlpha=quadMinAlpha;
							copyVectors (gs->quadMinAlphaPt, gs->midPt);
							gs->fmidPt=fquadMinAlpha;
							gs->endAlpha=quadMinAlpha+fabs(quadMinAlpha-gs->endAlpha)/gratio;
							vectorAdd (1.0, gs->startOrig, gs->endAlpha, gs->direction, gs->endPt);
							projectBox (gs->startOrig, gs->endPt, gs->direction, &gs->endAlpha, mads);
							gs->fendPt=singlePtEval (gs->endPt, mads, uData);
							gs->numItersBracket++;
						}
						/* Quadratic minimum trapped inside a bracket. Creating a bracket around it*/
						else
						{
							gs->startAlpha=gs->midAlpha;
							gs->fstartPt=gs->fmidPt;
							copyVectors (gs->midPt, gs->startPt);
							gs->midAlpha=quadMinAlpha;
							gs->fmidPt=fquadMinAlpha;
							copyVectors (gs->quadMinAlphaPt, gs->midPt);
							gs->midPtLength=gs->midAlpha-gs->startAlpha;
							gs->stepLength=gs->endAlpha-gs->startAlpha;
							// printf ("quadratic minima trapped inside bracket\n");
							// printf ("3. values of gs->fstartPt=%f, gs->fmidPt=%f and gs->fendPt=%f while stepLength=%f\n",gs->fstartPt,gs->fmidPt,gs->fendPt,gs->endAlpha-gs->startAlpha);
							return;
						}
					}
					else
					{
						//printVector(gs->endPt);
						//printVector(gs->quadMinAlphaPt);
						gs->startAlpha=gs->midAlpha;
						gs->midAlpha=gs->endAlpha;
						gs->endAlpha=quadMinAlpha;
						gs->fstartPt=gs->fmidPt;
						gs->fmidPt=gs->fendPt;
						gs->fendPt=fquadMinAlpha;
						copyVectors (gs->midPt, gs->startPt);
						copyVectors (gs->endPt, gs->midPt);
						copyVectors (gs->quadMinAlphaPt, gs->endPt);
						gs->midPtLength=gs->midAlpha-gs->startAlpha;
						gs->stepLength=gs->endAlpha-gs->startAlpha;
						// printf ("4. values of gs->fstartPt=%f, gs->fmidPt=%f and gs->fendPt=%f while stepLength=%f\n",gs->fstartPt,gs->fmidPt,gs->fendPt,gs->endAlpha-gs->startAlpha);
						return;
					}
				}
			}

			else if (gs->fmidPt > gs->fendPt)
			{
				gs->startAlpha=gs->midAlpha;
				gs->fstartPt=gs->fmidPt;
				gs->midAlpha=gs->endAlpha;
				gs->fmidPt=gs->fendPt;
				gs->endAlpha=(gs->midAlpha-gs->startAlpha)/gratio+gs->startAlpha;
				vectorAdd (1.0, gs->startOrig, gs->endAlpha, gs->direction, gs->endPt);
				projectBox (gs->startOrig, gs->endPt, gs->direction, &gs->endAlpha, mads);
				gs->fendPt=singlePtEval (gs->endPt, mads, uData);
				gs->numItersBracket++;
			}

			else  /*gs->fmidPt==gs->fendPt */
			{
				gs->endAlpha=gs->midAlpha;
				gs->fendPt=gs->fmidPt;
				gs->midAlpha=(gs->endAlpha-gs->startAlpha)*gratio+gs->startAlpha;
				vectorAdd (1.0, gs->startOrig, gs->midAlpha, gs->direction, gs->midPt);
				projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->midAlpha, mads);
				//gs->fmidPt=penaltyFuncValue (gs->midPt, lagParamStruct);
				gs->fmidPt=singlePtEval (gs->midPt, mads, uData);
				gs->numItersBracket++;
			}
		}
		/* gs->fmidPt >= gs->fstartPt. 
		   Future work: We may need to check in reverse gs->direction */
		else
		{
			gs->endAlpha=gs->midAlpha;
			gs->fendPt=gs->fmidPt;
			gs->midAlpha=(gs->endAlpha-gs->startAlpha)*gratio+gs->startAlpha;
			vectorAdd (1.0, gs->startOrig, gs->midAlpha, gs->direction, gs->midPt);
			projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->midAlpha, mads);
			gs->fmidPt=singlePtEval (gs->midPt, mads, uData);
			gs->numItersBracket++;
		}
		// printf ("value of gs->numItersBracket is %d\n",gs->numItersBracket);
	}
	while (gs->numItersBracket < gs->maxNumItersBracket && (gs->endAlpha-gs->startAlpha) > gs->epsilonBracket*((gs->startAlpha+gs->endAlpha)*0.5+gs->epsilonBracket));

	gs->midPtLength=gs->midAlpha-gs->startAlpha;
	gs->stepLength=gs->endAlpha-gs->startAlpha;
	// printf ("some issues with bracketing. Probably stepLength reduced to less than epsilon \n");
	// printf ("5. values of gs->fstartPt=%f, gs->midPt=%f and gs->fendPt=%f while stepLength=%f\n",gs->fstartPt,gs->fmidPt,gs->fendPt,gs->endAlpha-gs->startAlpha);
}
