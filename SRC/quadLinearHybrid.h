/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _QUADLINEARHYBRID_H_
#define _QUADLINEARHYBRID_H_
#include "quadInterpolate.h"
#include "quadHessianPositivity.h"
#include "simplexGradient.h"
#include "pollStep.h"
#include "vicinitySearch.h"
#include "Brent.h"
typedef struct
{
	vectorD *oldPt;
	vectorD *newPt;
	vectorD *oldGrad;
	vectorD *newGrad;
	vectorD *cgDirection;
	double cgEpsilon;
	int cgIterationMax;	
	vectorD *vicinityPt;
	vectorD *diffGrad;
	vectorD *diffPts;
	matrixD *thetaMatrix;
}scgStructQL;

void scgInitQL (scgStructQL *conjGrad, int dimension);
void quadScgMadsQL (madsStructure *mads, scgStructQL *conjGrad, simplexGradStruct *sg, lineSearchStruct *gs, userData *uData);
void freeSCGQL (scgStructQL *conjGrad);
#endif
