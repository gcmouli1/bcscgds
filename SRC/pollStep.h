/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _POLLSTEP_H
#define _POLLSTEP_H
#include "madsDeclare.h"
#include "Halton.h"
#include "lineSearch.h"
#include "simplexGradient.h"
#include "Brent.h"
//void pollStep (madsStructure *mads, vectorD *point);
void pollStep (vectorD *point, madsStructure *mads, double radius, int numPollSteps, userData *uData);
#endif
