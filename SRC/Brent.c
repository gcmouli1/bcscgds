/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "Brent.h"
double funcEvalAccept (lineSearchStruct *gs, vectorD *currentPt, madsStructure *mads, userData *uData)
{
	gs->previousBest=mads->bestVal;
	mads->lineSearchPoint=true;
//	int i;
//	for (i=0; i<mads->dimension; i++)
//	{
//		if (currentPt->vec[i] < mads->varLB->vec[i] || currentPt->vec[i] > mads->varUB->vec[i])
//		{
//			// printf("currentPt is infeasible\n");
//			exit(10);
//		}
//	}
	double funcValue=singlePtEval (currentPt, mads, uData);
	// printf ("values of previousBest=%f, currentbest=%f and startValue=%f\n",gs->previousBest, funcValue, gs->startPtValue);
	double epsilon=1E-14;
	double improvePercent=fabs((gs->previousBest-funcValue)/(gs->startPtValue-funcValue+epsilon));
	// printf ("percentage improvement is %e\n",improvePercent);
	gs->lineSearchStop=false;

	if (improvePercent < 1E-3)
	{
		gs->lineSearchStop = true;
		// printf ("stopping line search because of lack of sustantial improvement\n");
	}
	else
	{
		gs->lineSearchStop=false;
	}
	mads->lineSearchPoint=false;

	return funcValue;
}

void minimumBracket (lineSearchStruct *gs, bool outsideRange, bool *cornerMinima)
{
	double tmpValue=INFS;
	int minimaIndex=-1;
	int i=0;
	int size;
	gs->bracketIndex[0]=0;
	gs->bracketIndex[1]=-1;
	gs->bracketIndex[2]=3;

	if (outsideRange == false)
	{
		size=5;
	}
	else
	{
		size=4;
	}

	for (i=0; i<size; i++)
	{
		if (tmpValue > gs->funcValArray[i])
		{
			tmpValue=gs->funcValArray[i];
			minimaIndex=i;
		}
	}

	gs->bracketIndex[1]=minimaIndex;
	// printf ("printing minimaIndex=%d\n",minimaIndex);

	if (minimaIndex == 0 || minimaIndex == 3)
	{
		// printf("Minima point lies at the starting or ending point. No more bracket exists \n");
		*cornerMinima=true;
		return;
	}

	for (i=0; i<size; i++)
	{
		if (i != minimaIndex)
		{
			if (gs->pointsArray[minimaIndex] > gs->pointsArray[i] && gs->pointsArray[gs->bracketIndex[0]] <= gs->pointsArray[i])
			{
				gs->bracketIndex[0]=i;
			}

			if (gs->pointsArray[minimaIndex] < gs->pointsArray[i] && gs->pointsArray[gs->bracketIndex[2]] >= gs->pointsArray[i])
			{
				gs->bracketIndex[2]=i;
			}
		}
	}
}

void parabolaFit (bool *collinear, lineSearchStruct *gs, madsStructure *mads, userData *uData) 
{
	if (fabs((gs->funcValArray[gs->bracketIndex[1]]-gs->funcValArray[gs->bracketIndex[0]])*(gs->pointsArray[gs->bracketIndex[2]]-gs->pointsArray[gs->bracketIndex[0]])-(gs->funcValArray[gs->bracketIndex[2]]-gs->funcValArray[gs->bracketIndex[0]])*(gs->pointsArray[gs->bracketIndex[1]]-gs->pointsArray[gs->bracketIndex[0]]) <= gs->epsilonParabolic))
	{
		// printf ("all points almost on a straight line\n");
		*collinear=true;
	}
	/* Fit a quadratic over these points */
	else
	{
		// printf ("computing parabolic minima/maxima \n");
		gs->alphaMatrix->mat[0]=pow(gs->pointsArray[gs->bracketIndex[0]],2);
		gs->alphaMatrix->mat[1]=pow(gs->pointsArray[gs->bracketIndex[1]],2);
		gs->alphaMatrix->mat[2]=pow(gs->pointsArray[gs->bracketIndex[2]],2);
		gs->alphaMatrix->mat[3]=gs->pointsArray[gs->bracketIndex[0]];
		gs->alphaMatrix->mat[4]=gs->pointsArray[gs->bracketIndex[1]];
		gs->alphaMatrix->mat[5]=gs->pointsArray[gs->bracketIndex[2]];
		gs->alphaMatrix->mat[6]=1;
		gs->alphaMatrix->mat[7]=1;
		gs->alphaMatrix->mat[8]=1;

		gs->funcVector->vec[0]=gs->funcValArray[gs->bracketIndex[0]];
		gs->funcVector->vec[1]=gs->funcValArray[gs->bracketIndex[1]];
		gs->funcVector->vec[2]=gs->funcValArray[gs->bracketIndex[2]];
		int info;
		linearSystem (gs->alphaMatrix, gs->funcVector, gs->quadFitCoeff, &info);
		if (info != 0)
		{
			exit(10);
		}
		double quadMinAlpha=-gs->quadFitCoeff->vec[1]/(2*gs->quadFitCoeff->vec[0]);
		vectorAdd (1.0, gs->startOrig, quadMinAlpha, gs->direction, gs->quadMinAlphaPt);
		projectBox (gs->startOrig, gs->quadMinAlphaPt, gs->direction, &quadMinAlpha, mads);
		double quadMin=funcEvalAccept (gs, gs->quadMinAlphaPt, mads, uData);
		if (mads->iterLimitCrossed==true)
		{
			return;
		}

		if (gs->lineSearchStop == true)
		{
			return;
		}
		if (quadMin < gs->funcValArray[4]) 
		{
			gs->pointsArray[4]=quadMinAlpha;
			gs->funcValArray[4]=quadMin;
		}
	}
}

void brent (lineSearchStruct *gs, madsStructure *mads, vectorD *startPt, vectorD *direction, userData *uData)
{
	// printf ("Beginning the Brent algorithm\n");
	mads->numLineSearch++;
	copyVectors (startPt, gs->startPt);
	copyVectors (direction, gs->direction);
//	// printf ("printing direction in Brent \n");
//	printVector (gs->direction);
	vectorNormalize2 (gs->direction);
	//	// printf ("printing direction in Brent after Normalization\n");
	//	printVector (gs->direction);

	gs->midPtLength=gs->stepLength;
	double gratio=0.381;
	bracket (gs, mads, uData);
	if (mads->iterLimitCrossed==true)
	{
		return;
	}
	bool optCondition=0;
	gs->numItersBrent=0;
	gs->pointsArray[0]=gs->startAlpha;
	gs->pointsArray[3]=gs->endAlpha;
	gs->pointsArray[1]=gs->pointsArray[0]+gratio*(gs->stepLength);
	gs->pointsArray[2]=gs->pointsArray[3]-gratio*(gs->stepLength);
	gs->pointsArray[4]=gs->midAlpha;

	gs->startPtValue=gs->fstartPt;
	gs->funcValArray[0]=gs->fstartPt;
	gs->funcValArray[3]=gs->fendPt;
	vectorAdd (1.0, gs->startOrig, gs->pointsArray[1], gs->direction, gs->midPt);
	projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[1], mads);
	gs->funcValArray[1]=funcEvalAccept(gs, gs->midPt, mads, uData);
	if (mads->iterLimitCrossed==true)
	{
		return;
	}
	if (gs->lineSearchStop == true)
	{
		return;
	}
	vectorAdd (1.0, gs->startOrig, gs->pointsArray[2], gs->direction, gs->midPt);
	projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[2], mads);
	gs->funcValArray[2]=funcEvalAccept(gs, gs->midPt, mads, uData);
	if (mads->iterLimitCrossed==true)
	{
		return;
	}
	if (gs->lineSearchStop == true)
	{
		return;
	}
	gs->funcValArray[4]=gs->fmidPt;

	int i;

	// printf("printing initial values of a,b,w1,w2,p and corresponding fa,fb,fw1,fw2,fp\n");
	for (i=0; i<5; i++)
	{
		// printf ("%f\t%f\n",gs->pointsArray[i],gs->funcValArray[i]);
	}

	bool outsideRange=0;
	bool collinear=0;
	bool cornerMinima=0;
	minimumBracket (gs, outsideRange, &cornerMinima);

	// printf("printing gs->bracketIndex array\n");
	for (i=0; i<3; i++)
	{
		// printf("%d\t",gs->bracketIndex[i]);
	}
	// printf("\n");

	while (optCondition == false && gs->numItersBrent < gs->maxNumItersBrent)
	{
		if (gs->stepLength < gs->epsilonBrent)
		{
			optCondition=1;
		}
		gs->pointsArray[0]=gs->pointsArray[gs->bracketIndex[0]];
		gs->pointsArray[3]=gs->pointsArray[gs->bracketIndex[2]];
		gs->funcValArray[0]=gs->funcValArray[gs->bracketIndex[0]];
		gs->funcValArray[3]=gs->funcValArray[gs->bracketIndex[2]];
		gs->stepLength=gs->pointsArray[1]-gs->pointsArray[0];

		if (outsideRange == true) // pure Golden Section
		{
			if (gs->funcValArray[1] < gs->funcValArray[0] && gs->funcValArray[2] < gs->funcValArray[3])
			{
				// printf ("Both w1 and w2 are low \n");
				gs->pointsArray[0]=gs->pointsArray[1];
				gs->funcValArray[0]=gs->funcValArray[1];
				gs->pointsArray[3]=gs->pointsArray[2];
				gs->funcValArray[3]=gs->funcValArray[2];
				gs->stepLength=gs->pointsArray[3]-gs->pointsArray[0];
				gs->pointsArray[1]=gs->pointsArray[0]+gratio*(gs->stepLength);
				gs->pointsArray[2]=gs->pointsArray[3]-gratio*(gs->stepLength);
				vectorAdd (1.0, gs->startOrig, gs->pointsArray[1], gs->direction, gs->midPt);
				projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[1], mads);
				gs->funcValArray[1]=funcEvalAccept(gs, gs->midPt, mads, uData);
				if (mads->iterLimitCrossed==true)
				{
					return;
				}
				if (gs->lineSearchStop == true)
				{
					break;
				}
				vectorAdd (1.0, gs->startOrig, gs->pointsArray[2], gs->direction, gs->midPt);
				projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[2], mads);
				gs->funcValArray[2]=funcEvalAccept(gs, gs->midPt, mads, uData);
				if (mads->iterLimitCrossed==true)
				{
					return;
				}
				if (gs->lineSearchStop == true)
				{
					break;
				}
			}
			else if (gs->funcValArray[1] > gs->funcValArray[2])
			{
				// printf ("fw1 is greater than fw2 \n");
				gs->pointsArray[0]=gs->pointsArray[1];
				gs->funcValArray[0]=gs->funcValArray[1];
				gs->pointsArray[1]=gs->pointsArray[2];
				gs->funcValArray[1]=gs->funcValArray[2];
				gs->stepLength=gs->pointsArray[3]-gs->pointsArray[0];
				gs->pointsArray[2]=gs->pointsArray[3]-gratio*(gs->stepLength);
				vectorAdd (1.0, gs->startOrig, gs->pointsArray[2], gs->direction, gs->midPt);
				projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[2], mads);
				gs->funcValArray[2]=funcEvalAccept(gs, gs->midPt, mads, uData);
				if (mads->iterLimitCrossed==true)
				{
					return;
				}
				if (gs->lineSearchStop == true)
				{
					break;
				}
			}
			else if(gs->funcValArray[1] < gs->funcValArray[2])
			{
				// printf ("fw2 is greater than fw1 \n");
				gs->pointsArray[3]=gs->pointsArray[2];
				gs->funcValArray[3]=gs->funcValArray[2];
				gs->pointsArray[2]=gs->pointsArray[1];
				gs->funcValArray[2]=gs->funcValArray[1];
				gs->stepLength=gs->pointsArray[3]-gs->pointsArray[0];
				gs->pointsArray[1]=gs->pointsArray[0]+gratio*(gs->stepLength);
				vectorAdd (1.0, gs->startOrig, gs->pointsArray[1], gs->direction, gs->midPt);
				projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[1], mads);
				gs->funcValArray[1]=funcEvalAccept(gs, gs->midPt, mads, uData);
				if (mads->iterLimitCrossed==true)
				{
					return;
				}
				if (gs->lineSearchStop == true)
				{
					break;
				}
			}
			else
			{
				// printf ("Some issues with bracketing and Golden Section \n");
			}
		}
		else // Golden section with Parabolic fitting
		{
			gs->stepLength=gs->pointsArray[3]-gs->pointsArray[0];
			gs->pointsArray[1]=gs->pointsArray[0]+gratio*(gs->stepLength);
			gs->pointsArray[2]=gs->pointsArray[3]-gratio*(gs->stepLength);
			vectorAdd (1.0, gs->startOrig, gs->pointsArray[1], gs->direction, gs->midPt);
			projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[1], mads);
			gs->funcValArray[1]=funcEvalAccept(gs, gs->midPt, mads, uData);
			if (mads->iterLimitCrossed==true)
			{
				return;
			}
			if (gs->lineSearchStop == true)
			{
				break;
			}
			vectorAdd (1.0, gs->startOrig, gs->pointsArray[2], gs->direction, gs->midPt);
			projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[2], mads);
			gs->funcValArray[2]=funcEvalAccept(gs, gs->midPt, mads, uData);
			if (mads->iterLimitCrossed==true)
			{
				return;
			}
			if (gs->lineSearchStop == true)
			{
				break;
			}
		}

		parabolaFit (&collinear, gs, mads, uData);
		if (gs->lineSearchStop == true)
		{
			break;
		}
		if (gs->pointsArray[4] < gs->pointsArray[0] || gs->pointsArray[4] > gs->pointsArray[3]) 
		{
			outsideRange=true;
		}
		// printf ("printing outsideRange value=%d\n",outsideRange);
		// printf("printing intermediate values of a,b,w1,w2,p and corresponding fa,fb,fw1,fw2,fp\n");
		for (i=0; i<5; i++)
		{
			// printf ("%f\t%f\n",gs->pointsArray[i],gs->funcValArray[i]);
		}

		minimumBracket (gs, outsideRange, &cornerMinima);
		if (cornerMinima == true)
		{
			return;
		}

		// printf("printing gs->bracketIndex array\n");
		for (i=0; i<3; i++)
		{
			// printf("%d\t",gs->bracketIndex[i]);
		}
		// printf("\n");
		gs->numItersBrent++;
	}
	vectorAdd (1.0, gs->startOrig, gs->pointsArray[gs->bracketIndex[1]], gs->direction, gs->midPt);
	projectBox (gs->startOrig, gs->midPt, gs->direction, &gs->pointsArray[gs->bracketIndex[1]], mads);
	// printf ("Iterations in Brent algorithm=%d\n",gs->numItersBrent);
}
