/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _REMOVEDUPLICATE_H
#define _REMOVEDUPLICATE_H
#include "vecLapack.h"
#include "madsDeclare.h"
#include "../funcEval.h"
#include "funcConstrSync.h"
/*This function checks uniqueness of given points and evaluates function value at new points.
 *
*inputVecSet is a matrix of given points at which function has to be evaluated after checking uniqueness 
 * inputUniqueRepeatIndex stores indices of repeated points else it gives -1 for unique ones
 * allbadPts if 0 then one better value found 
 */

void penaltyUpdate (madsStructure *mads);
void rmDuplicate (matrixD *inputVecSet, int *inputUniqueRepeatIndex, madsStructure *mads, userData *uData);
#endif
