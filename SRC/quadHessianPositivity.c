/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "quadHessianPositivity.h"
void quadHessianPositivity (madsStructure *mads, vectorD *optimum)
{
	int dimension=mads->dimension;
	//matrixD *quadHessian=malloc (sizeof(matrixD));
	//matrixInit (quadHessian, dimension, dimension);
	int i,j;
	int k=dimension;
	for (i=0; i<dimension-1; i++)
	{
		for (j=i+1; j<dimension; j++)
		{
			mads->quadHessian->mat[i*dimension+j]=mads->quadAlphaQ->vec[k+j-(i+1)];
		}
		k +=dimension-i-1;
	}
	for (i=0; i<dimension; i++)
	{
		mads->quadHessian->mat[i+i*dimension]=mads->quadAlphaQ->vec[i];
	}
	upperLowerFull ('U', mads->quadHessian);
	//	// printf ("printing mads->quadHessian\n");
	//	printMatrix (mads->quadHessian);
	vectorD *eigenValues=malloc (sizeof(vectorD));
	vectorInit (eigenValues, mads->quadHessian->rows);
	eigenValueCompute (mads->quadHessian, eigenValues);
	mads->quadFitPSD=true;
	for (i=0; i<eigenValues->dimension; i++)
	{
		if (eigenValues->vec[i] < 1E-5 )
		{
			mads->quadFitPSD=false;
		}
	}

	if (mads->quadFitPSD==false)
	{
		// printf ("quad fit is not psd matrix\n");
	}
	else
	{
		// printf ("quad fit is psd matrix\n");
	}
	//	// printf ("printing eigen values\n");
	//printVector (eigenValues);

	//	bool ptInfeasible=false;
	if (mads->quadFitPSD==true)
	{
		double optVal;
		//		vectorD *b=malloc (sizeof(vectorD));
		//		vectorInit (b, dimension); 
		memcpy (mads->quadLinear->vec, mads->quadAlphaL->vec+1, sizeof(double)*dimension);
		scaleVector (mads->quadLinear, -1.0);
		//	scaleMatrix (mads->quadHessian, 0.5);
		//	// printf("printing b vector \n");
		//printVector (mads->quadLinear);
		//	{
		int info;
		linearSystem (mads->quadHessian, mads->quadLinear, optimum, &info);
		if (info != 0)
		{
			exit(10);
		}
		optVal=quadFuncValue (mads, optimum);
		// printf ("printing unconstrained quadratic min =%lf with vector\n",optVal);
		//	printVector (optimum);
		//	}

		for (i=0; i<dimension; i++)
		{
			if (optimum->vec[i] < mads->varLB->vec[i] || optimum->vec[i] > mads->varUB->vec[i])
			{
				mads->ptInfeasible=true;
			}
		}
		if (mads->ptInfeasible==true)
		{
			// printf ("point is infeasible. Need gradient projection\n");
		}
		//		if (mads->ptInfeasible==true)
		//		{
		//			// printf ("point is infeasible. Need gradient projection\n");
		//			optVal=unConstraintedOptimQuad (optimum, mads);
		//			mads->ptInfeasible=true;
		//		}
		// printf ("printing quadratic min func value=%lf with vector\n", optVal);
		//printVector (optimum);
		//freeVectorD (b);
	}

	//freeMatrixD (mads->quadHessian);
	freeVectorD (eigenValues);
}

//void gradientQuad (vectorD *point, madsStructure *mads)
//{
//	vectorMatrixMult ('N', 1.0, mads->quadHessian, point, -1.0, mads->quadLinear, mads->quadGradient);
//}

void projection (vectorD *point, madsStructure *mads, vectorD *pointProjection)
{
	int i;
	copyVectors (point, pointProjection);
	for (i=0; i<mads->dimension; i++)
	{
		if (point->vec[i] < mads->varLB->vec[i])
		{
			pointProjection->vec[i]=mads->varLB->vec[i];
		}
		if (point->vec[i] > mads->varUB->vec[i])
		{
			pointProjection->vec[i]=mads->varUB->vec[i];
		}
	}
}

void bracketingQuad (vectorD *startPt, vectorD *direction, vectorD *midPt, vectorD *endPt, double *stepLength, double *fstartPtOrig, double *fendPtOrig, madsStructure *mads)
{
	int numIters=0;
	int maxNumIters=100;
	vectorD *startOrig=malloc (sizeof(vectorD));
	vectorInit (startOrig, startPt->dimension);
	copyVectors (startPt, startOrig);
	vectorNormalize2 (direction);
	double gratio=0.3819660;
	double startAlpha, midAlpha, endAlpha;
	double fstartPt, fmidPt, fendPt;
	double epsilon=1e-7;
	startAlpha=0;
	endAlpha=*stepLength;
	vectorAdd (1.0, startOrig, endAlpha, direction, endPt);
	//	// printf("printing startPt in bracketing\n");
	//	printVector (startPt);
	fstartPt=quadFuncValue (mads, startPt);
	fendPt=quadFuncValue (mads, endPt);
	//	// printf ("values of fstartPt=%lf\n",fstartPt);

	if (*stepLength*0.5 < epsilon)
	{
		if (fabs(fendPt-fstartPt) < epsilon)
		{
			// printf("no need for bracketing. Almost flat surface or optimum\n");
			*fstartPtOrig=fstartPt;
			*fendPtOrig=fendPt;
			// printf ("values of fstartPt=%f, fmidPt=%f and fendPt=%f while stepLength=%f\n",fstartPt,fmidPt,fendPt,endAlpha-startAlpha);
			return;
		}
	}

	if (fendPt >= fstartPt)
	{
		midAlpha=(endAlpha-startAlpha)*gratio+startAlpha;
		vectorAdd (1.0, startOrig, midAlpha, direction, midPt);
		fmidPt=quadFuncValue (mads, midPt);
	}
	else
	{
		midAlpha=endAlpha;
		fmidPt=fendPt;
		endAlpha=(endAlpha-startAlpha)/gratio+startAlpha;
		vectorAdd (1.0, startOrig, endAlpha, direction, endPt);
		fendPt=quadFuncValue (mads, endPt);
	}

	do
	{
		if (fmidPt < fstartPt)
		{
			if (fmidPt < fendPt)
			{
				*stepLength=endAlpha-startAlpha;
				*fstartPtOrig=fstartPt;
				*fendPtOrig=fendPt;
				// printf ("values of fstartPt=%f, fmidPt=%f and fendPt=%f while stepLength=%f\n",fstartPt,fmidPt,fendPt,endAlpha-startAlpha);
				return;
			}

			else if (fmidPt > fendPt)
			{
				startAlpha=midAlpha;
				fstartPt=fmidPt;
				midAlpha=endAlpha;
				fmidPt=fendPt;
				endAlpha=(midAlpha-startAlpha)/gratio+startAlpha;
				vectorAdd (1.0, startOrig, endAlpha, direction, endPt);
				fendPt=quadFuncValue (mads, endPt);
				numIters++;
			}

			else  /*fmidPt==fendPt */
			{
				endAlpha=midAlpha;
				fendPt=fmidPt;
				midAlpha=(endAlpha-startAlpha)*gratio+startAlpha;
				vectorAdd (1.0, startOrig, midAlpha, direction, midPt);
				fmidPt=quadFuncValue (mads, midPt);
				numIters++;
			}
		}
		else /* fmidPt >= fstartPt */
		{
			endAlpha=midAlpha;
			fendPt=fmidPt;
			midAlpha=(endAlpha-startAlpha)*gratio+startAlpha;
			vectorAdd (1.0, startOrig, midAlpha, direction, midPt);
			fmidPt=quadFuncValue (mads, midPt);
			numIters++;
		}
	}
	while (numIters < maxNumIters && (endAlpha-startAlpha) > epsilon);

	*stepLength=endAlpha-startAlpha;
	*fstartPtOrig=fstartPt;
	*fendPtOrig=fendPt;
	// printf ("some issues with bracketing. Probably stepLength reduced to less than epsilon \n");
	// printf ("values of fstartPt=%f, fmidPt=%f and fendPt=%f while stepLength=%f\n",fstartPt,fmidPt,fendPt,endAlpha-startAlpha);
	exit(10);
}

/* Golden Section from K.Deb book. Pg. 47*/
void lineSearchQuad (vectorD *direction, vectorD *startPt, vectorD *optPt, vectorD *endPt, double oldStepLength, madsStructure *mads)
{
	double stepLength=oldStepLength;
	double fstartPt, fendPt;
	bracketingQuad (startPt, direction, optPt, endPt, &stepLength, &fstartPt, &fendPt, mads);
	double a=0;
	double b=stepLength;
	double w1=a+0.618*(b-a);
	double w2=b-0.618*(b-a);
	bool optCondition=0;
	double fa, fb, fw1, fw2;
	double epsilon=1E-5;
	int numIters=0;
	int maxNumIters=100;
	fa=fstartPt;
	fb=fendPt;
	vectorAdd (1.0, startPt, w1, direction, optPt);
	fw1=quadFuncValue (mads, optPt);
	vectorAdd (1.0, startPt, w2, direction, optPt);
	fw2=quadFuncValue (mads, optPt);
	// printf("printing initial values of fa=%f, fb=%f, fw1=%f, fw2=%f\n",fa,fb,fw1,fw2);

	while (optCondition == false && numIters < maxNumIters)
	{
		if (b-a < epsilon)
		{
			optCondition=1;
		}
		else if (fw1 < fa && fw2 < fb)
		{
			a=w2;
			b=w1;
			fa=fw2;
			fb=fw1;
			w1=a+0.618*(b-a);
			w2=b-0.618*(b-a);
			vectorAdd (1.0, startPt, w1, direction, optPt);
			fw1=quadFuncValue (mads, optPt);
			vectorAdd (1.0, startPt, w2, direction, optPt);
			fw2=quadFuncValue (mads, optPt);
		}
		else if (fw2 > fw1)
		{
			a=w2;
			fa=fw2;
			w2=w1;
			fw2=fw1;
			w1=a+0.618*(b-a);
			vectorAdd (1.0, startPt, w1, direction, optPt);
			fw1=quadFuncValue (mads, optPt);
		}
		else if (fw2 < fw1)
		{
			b=w1;
			fb=fw1;
			w1=w2;
			fw1=fw2;
			w2=b-0.618*(b-a);
			vectorAdd (1.0, startPt, w2, direction, optPt);
			fw2=quadFuncValue (mads, optPt);
		}
		else
		{
			// printf("some issues with bracketing probably \n");
			exit(10);
		}
		numIters++;
	}

	if (fw1 < fw2)
	{
		vectorAdd (1.0, startPt, w1, direction, optPt);
		fw1=quadFuncValue (mads, optPt);
	}
	else
	{
		vectorAdd (1.0, startPt, w2, direction, optPt);
		fw2=quadFuncValue (mads, optPt);
	}

	// printf("best value from line search: fw1=%f and fw2=%f \n", fw1, fw2);
}

/* Steepest Descent taken from K. Deb book */
double unConstraintedOptimQuad (vectorD *startPt, madsStructure *mads)
{
	int maxNumIters=100;
	int numIters=0;
	double epsilon=1E-5;
	bool optCondition=0;
	vectorD *nextPt=malloc (sizeof(vectorD));
	vectorInit (nextPt, startPt->dimension);
	vectorD *nextPtProj=malloc (sizeof(vectorD));
	vectorInit (nextPtProj, startPt->dimension);
	vectorD *gradient=malloc (sizeof(vectorD));
	vectorInit (gradient, startPt->dimension);
	vectorD *tmp=malloc (sizeof(vectorD));
	vectorInit (tmp, startPt->dimension);
	//	// printf("printing dimension of rbfSolution in unConstraintedOptim %d\n",mads->rbfSolution->dimension);
	//	gradientVectorQuad (startPt, gradient, lagParamQuad, mads);
	vectorMatrixMult ('N', 1.0, mads->quadHessian, startPt, -1.0, mads->quadLinear, gradient);
	double stepLength=1;

	while (optCondition == false && numIters < maxNumIters)
	{
		scaleVector(gradient, -1.0);
		lineSearchQuad (gradient, startPt, nextPt, tmp, stepLength, mads);
		projection (nextPt, mads, nextPtProj);
		copyVectors (nextPtProj, nextPt);
		vectorMatrixMult ('N', 1.0, mads->quadHessian, nextPt, -1.0, mads->quadLinear, gradient);
		vectorAdd (1.0, nextPt, -1.0, startPt, tmp);
		stepLength=vectorNorm2 (tmp);
		if (stepLength/vectorNorm2(startPt) <= epsilon)
		{
			optCondition=1;
		}
		numIters++;
		copyVectors (nextPt, startPt);
	}
	freeVectorD (gradient);
	freeVectorD (tmp);
	freeVectorD (nextPt);
	freeVectorD (nextPtProj);

	return quadFuncValue (mads, startPt);
}




