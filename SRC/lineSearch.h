/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _LINESEARCH_H_
#define _LINESEARCH_H_
#include "vecLapack.h"

typedef struct 
{
	bool lineSearchStop;
	double previousBest;
	double improvementPercent;
	double startPtValue;
	vectorD *startPt;
	vectorD *startOrig;
	vectorD *midPt;
	vectorD *endPt;
	vectorD *direction;
	matrixD *alphaMatrix;
	vectorD *quadFitCoeff;
	vectorD *quadMinAlphaPt;
	vectorD *funcVector;
	double stepLength;
	double midPtLength;
	double startAlpha;
	double midAlpha;
	double endAlpha;
	double fstartPt;
	double fmidPt;
	double fendPt;
	int maxNumItersBrent;
	int numItersBrent;
	int maxNumItersBracket;
	int numItersBracket;
	double epsilonBrent;
	double epsilonParabolic;
	double epsilonBracket;
	double pointsArray[5]; //stores gs->startPt, gs->endPt, w1, w2 and parabolic minima pt.
	double funcValArray[5];
	int bracketIndex[3]; //stores indices of bracket from gs->pointsArray
} lineSearchStruct;

void lineSearchInitiate (lineSearchStruct *gs, vectorD *initialPt, vectorD *direction, int maxNumItersBrent, int maxNumItersBracket);
void freeLineSearch (lineSearchStruct *gs);
#endif
