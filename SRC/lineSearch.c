/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "lineSearch.h"

void lineSearchInitiate (lineSearchStruct *gs, vectorD *initialPt, vectorD *direction, int maxNumItersBrent, int maxNumItersBracket)
{
	gs->startPt = malloc (sizeof(vectorD));
	vectorInit (gs->startPt, initialPt->dimension);
	gs->startOrig = malloc (sizeof(vectorD));
	vectorInit (gs->startOrig, initialPt->dimension);
	copyVectors (initialPt, gs->startPt);
	copyVectors (initialPt, gs->startOrig);
	
	gs->direction = malloc (sizeof(vectorD));
	vectorInit (gs->direction, direction->dimension);
	copyVectors (direction, gs->direction);

	gs->midPt = malloc (sizeof(vectorD));
	vectorInit (gs->midPt, initialPt->dimension);
	gs->endPt = malloc (sizeof(vectorD));
	vectorInit (gs->endPt, initialPt->dimension);

	gs->alphaMatrix = malloc (sizeof(matrixD));
	matrixInit (gs->alphaMatrix, 3, 3);

	gs->quadFitCoeff = malloc (sizeof(vectorD));
	vectorInit (gs->quadFitCoeff, 3);

	gs->quadMinAlphaPt = malloc (sizeof(vectorD));
	vectorInit (gs->quadMinAlphaPt, initialPt->dimension);

	gs->funcVector = malloc (sizeof(vectorD));
	vectorInit (gs->funcVector, 3);
	
	gs->stepLength=1;
	gs->midPtLength=gs->stepLength/2.0;

	gs->fstartPt=1E10;
	gs->fmidPt=1E10;
	gs->fendPt=1E10;

	gs->maxNumItersBrent=maxNumItersBrent;
	gs->maxNumItersBracket=maxNumItersBracket;

	gs->epsilonBrent=1E-5;
	gs->epsilonBracket=1E-5;
	gs->epsilonParabolic=1E-7;
}

void freeLineSearch (lineSearchStruct *gs)
{
	freeVectorD (gs->startPt);
	freeVectorD (gs->startOrig);
	freeVectorD (gs->midPt);
	freeVectorD (gs->endPt);
	freeVectorD (gs->direction);
	freeVectorD (gs->quadFitCoeff);
	freeVectorD (gs->quadMinAlphaPt);
	freeVectorD (gs->funcVector);
	freeMatrixD (gs->alphaMatrix);
	free (gs);
}


