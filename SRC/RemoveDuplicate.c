/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "RemoveDuplicate.h"

void penaltyUpdate (madsStructure *mads)
{
	int numElem=mads->numFeasibleElem;
	// printf ("printing number of feasible elements %d\n",numElem);
	int i;
	for (i=0; i<numElem; i++)
	{
		mads->funcSet->vec[i]=mads->funcSetActual->vec[i]+mads->penaltyParam*(mads->constrSet->vec[i]);
	}

	double value=DBL_MAX;
	int index=-1;
	for (i=0; i<numElem; i++)
	{
		if (mads->funcSet->vec[i] < value)
		{
			value=mads->funcSet->vec[i];
			index=i;
		}
	}
	mads->bestVal=value;
	mads->bestPtIndex=index;
	// printf ("printing bestvalue inside penalty update function %G with index %d\n",value,index);
	vecExtractMat (mads->bestPt, mads->ptSet, index);


}

void rmDuplicate (matrixD *inputVecSet, int *inputUniqueRepeatIndex, madsStructure *mads, userData *uData)
{
	//	int dimension = inputVecSet->rows;
	vectorD *inputVecSetHash1 = malloc(sizeof(vectorD));
	vectorD *inputVecSetHash2 = malloc(sizeof(vectorD));
	vectorD *inputVecSetHash3 = malloc(sizeof(vectorD));
	vectorInit (inputVecSetHash1, inputVecSet->cols);
	vectorInit (inputVecSetHash2, inputVecSet->cols);
	vectorInit (inputVecSetHash3, inputVecSet->cols);
	vectorMatrixMultOrig ('T', 1.0, inputVecSet, mads->vecHash1, 0.0, inputVecSetHash1);
	vectorMatrixMultOrig ('T', 1.0, inputVecSet, mads->vecHash2, 0.0, inputVecSetHash2);
	vectorMatrixMultOrig ('T', 1.0, inputVecSet, mads->vecHash3, 0.0, inputVecSetHash3);
	int i,j;
	double epsilon = 1E-6;

//	printf ("printing numFeasibleElem at beginning of RemoveDuplicate %d\n",mads->numFeasibleElem);
//	printf("number of points send %d\n",inputVecSet->cols);
//	printf ("printing points send for function evaluation\n");
//	printMatrix (inputVecSet);

	/*make sure that inputUniqueRepeatIndex is initiated with -1. */
	for (i = 0; i < inputVecSet->cols; i++)
	{
		inputUniqueRepeatIndex[i] = -1;
	}

	bool matchFound = 0;
	int numRepeatedVectors = 0;

	/*Check uniqueness of given set of vectors inputVecSet from ptSet using hash vectors ptSetHash1 and 2
	 * Check in reverse order since its possibility is high
	 */

	//if (inputVecSet->cols == 1)
//	{
	//	printf ("inputVecSetHash %1.10f\t%1.10f\t%1.10f\n",inputVecSetHash1->vec[0],inputVecSetHash2->vec[0],inputVecSetHash3->vec[0]);
	//	printMatrix (inputVecSet);
//	}
	//printf ("printing mads->numElem %d\n",mads->numElem);
	for (i = 0; i < inputVecSetHash1->dimension; i++)
	{
		j = mads->numElem-1;
		matchFound = 0;
		//while(j < vecHash1->dimension && matchFound == 0)  //size_t bounces to 2^N-1 at j < 0 so j < dimension
		while(j >= 0 && matchFound == false)  
		{
		//	printf ("ptSetHash i=%d\tj=%d\t%1.10f\t%1.10f\t%1.10f\n",i,j,mads->ptSetHash1->vec[j],mads->ptSetHash2->vec[j],mads->ptSetHash3->vec[j]);
			if (fabs (inputVecSetHash1->vec[i] - mads->ptSetHash1->vec[j]) < epsilon) 
			{
				if (fabs (inputVecSetHash2->vec[i] - mads->ptSetHash2->vec[j]) < epsilon)
				{
					if (fabs (inputVecSetHash3->vec[i] - mads->ptSetHash3->vec[j]) < epsilon)
					{
					//	printf ("Hit\n");
					//	printf ("inputVecSetHash %1.10f\t%1.10f\t%1.10f\n",inputVecSetHash1->vec[i],inputVecSetHash2->vec[i],inputVecSetHash3->vec[i]);
			//			printf ("ptSetHash %lf\t%lf\t%lf\n",mads->ptSetHash1->vec[j],mads->ptSetHash2->vec[j],mads->ptSetHash3->vec[j]);
					//	printf("printing value of index %d\n",j);
						inputUniqueRepeatIndex[i] = j;
						matchFound = 1;
						numRepeatedVectors++;
					}
				}
			}
			j--;
		}
	}

	int count = 0;
	int p;
	//Finding index of vector sent for evaluation. 
	if (inputVecSet->cols == 1)
	{
		for (p = 0; p < inputUniqueRepeatIndex[0]; p++)
		{
			if (mads->ptSetHardFeasibility[p] == true)
			{
				count++;
			}
		}
		//	// printf("value of count is %d\n",count);
		if (count != 0)
		{
			//inputUniqueRepeatIndex[0] = count-1;
			inputUniqueRepeatIndex[0] = count;
		}
	}

	int numUniqueVectors = inputVecSet->cols - numRepeatedVectors;
	if (numUniqueVectors == 0)
	{
	//	printf ("Repeated vector. So no better point found. Keeping old indices intact\n");
		mads->allbadPts = 1;
		if (inputVecSet->cols == 1)
		{
		//	printf ("printing repeated vector\n");
		//	vecExtractMat (mads->tmpVec, mads->ptSet, inputUniqueRepeatIndex[0]);
		//	printVector (mads->tmpVec);
		}
		return;
	}

	double *funcValUniqueVectors = malloc (sizeof(double)*numUniqueVectors);
	double *funcValUniqueVectorsActual = malloc (sizeof(double)*numUniqueVectors);
	double *constraintValue = malloc (sizeof(double)*numUniqueVectors);
	//Initialize with large values incase something goes wrong with indexing later
	for (i = 0; i < numUniqueVectors; i++)
	{
		funcValUniqueVectors[i] = 1E20;
		funcValUniqueVectorsActual[i] = 1E20;
	}

	/*Compute best point index and value */
	bool betterPtFound = false;
	bool goodPtFound = false;

	for (i = 0; i < mads->numFeasibleElem; i++)
	{
		if (mads->bestVal > mads->funcSet->vec[i])
		{
			mads->bestVal = mads->funcSet->vec[i];
			mads->bestPtIndex = i;
		}
	}

	i = 0; //iterator for all unique elements
	//	int newVecCount = 0;

	/*Compute function values at new vectors such that max iteration limit is not crossed */
	//	int initNumElem = mads->numElem;
	int initNumFeasElem = mads->numFeasibleElem;
	int k = 0;  //iterator for feasible elements
	double tmpFuncVal;
	double tmpFuncValActual;
	double tmpBestVal = mads->bestVal;
	while (i < inputVecSet->cols && mads->numFeasibleElem < mads->iterLimit) 
	{
		if (inputUniqueRepeatIndex[i] == -1) //check whether the point is unique
		{
			inputUniqueRepeatIndex[i] = mads->numFeasibleElem;
			vecExtractMat (mads->tmpVec, inputVecSet, i);
			tmpFuncValActual = funcEvalBoxConstr (mads->tmpVec, mads, uData);
			tmpFuncVal = tmpFuncValActual + mads->penaltyParam * mads->constrValue;
		//	printf ("printing function values of point in Remove duplicate algorithm %lf\n",tmpFuncVal);
			if (mads->ptInfeasible == true)
			{
				mads->numInfeasibleElem++;
				mads->ptSetHardFeasibility[mads->numElem] = false;
				inputUniqueRepeatIndex[i] = 2*mads->iterLimit;
			}
			else
			{
				mads->ptSetHardFeasibility[mads->numElem] = true;
				if (mads->constrValue < 1E-15)
				{
					mads->ptSetSoftFeasibility[mads->numFeasibleElem]='F'; //feasible
				}
				else
				{
					mads->ptSetSoftFeasibility[mads->numFeasibleElem]='S'; //semi-feasible
				}
				mads->numFeasibleElem++;

				funcValUniqueVectors[k] = tmpFuncVal;
				funcValUniqueVectorsActual[k] = tmpFuncValActual;
				constraintValue[k] = mads->constrValue;
				if (mads->bestVal > tmpFuncVal)
				{
					mads->bestVal = tmpFuncVal;
					mads->bestPtIndex = initNumFeasElem+k;
					betterPtFound = true;
				}
				k++;
				if (tmpBestVal-pow(mads->radius,2)/mads->madsRecFactor2 > tmpFuncVal)
				{
					goodPtFound = true;
				}
				memcpy (mads->ptSet->mat+(mads->dimension)*(mads->numFeasibleElem-1), mads->tmpVec->vec, (mads->dimension)*sizeof(double));
			}
			mads->ptInfeasible = false;  //Restore back to feasibility for next iteration
			mads->ptSetHash1->vec[mads->numElem] = vectorDotProd (mads->tmpVec, mads->vecHash1);
			mads->ptSetHash2->vec[mads->numElem] = vectorDotProd (mads->tmpVec, mads->vecHash2);
			mads->ptSetHash3->vec[mads->numElem] = vectorDotProd (mads->tmpVec, mads->vecHash3);
			mads->numElem++;
		}
		i++;
	}


	if (mads->numFeasibleElem > initNumFeasElem)
	{
		memcpy (mads->funcSet->vec+initNumFeasElem, funcValUniqueVectors, sizeof(double)*(mads->numFeasibleElem-initNumFeasElem));
		memcpy (mads->funcSetActual->vec+initNumFeasElem, funcValUniqueVectorsActual, sizeof(double)*(mads->numFeasibleElem-initNumFeasElem));
		memcpy (mads->constrSet->vec+initNumFeasElem, constraintValue, sizeof(double)*(mads->numFeasibleElem-initNumFeasElem));
	}

	if (mads->numFeasibleElem == mads->iterLimit)
	{
		mads->iterLimitCrossed = true;
		// printf("maximum evaluation limit crossed. Stopping further evaluations \n");
	}

	//	if (mads->iterLimitCrossed)
	//	{
	//		// printf("maximum evaluation limit crossed. Stopping further evaluations \n");
	//	}

	if (betterPtFound == false)
	{
		mads->allbadPts = true;
	//	printf("No better point found \n");
	}
	else
	{
		mads->allbadPts = false;
	}

	if (goodPtFound == false)
	{
		mads->allWorsePts = true;
	//	printf("All worse points found \n");
	}
	else
	{
		mads->allWorsePts = false;
	}
	// printf("number of feasible = %d and total points = %d\n",mads->numFeasibleElem, mads->numElem);
	// printf("Best point index is %d \n",mads->bestPtIndex);
	// printf("Best point function value is %f \n",mads->bestVal);
	//	printMatrix (mads->ptSet);
	vecExtractMat (mads->bestPt, mads->ptSet, mads->bestPtIndex);
	//	printVector (mads->bestPt);
	//	freeVectorD (tmpVecNDim);
	freeVectorD (inputVecSetHash1);
	freeVectorD (inputVecSetHash2);
	freeVectorD (inputVecSetHash3);
	free (funcValUniqueVectors);
	free (funcValUniqueVectorsActual);
	free (constraintValue);
}

