/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _VECBLAS_H
#define _VECBLAS_H
#include "vecMatDeclare.h"
/*Fortran BLAS functions */
void dswap_(int *dimension, double *vecX, int *incx, double *vecY, int *incy);
void dcopy_(int *dimension, double *vecX, int *incx, double *vecY, int *incy);
void dscal_(int *dimension, double *alpha, double *vecX, int *incx);
void daxpy_(int *dimension, double *alpha, double *vecX, int *incx, double *vecO, int *incy);
double ddot_(int *dimension, double *vecX, int *incx, double *vecY, int *incy);
double dnrm2_(int *dimension, double *vec, int *incx);
double dasum_(int *dimension, double *vec, int *incx);
int idamax_(int *dimension, double *vec, int *incx);
void dgemv_(char *Trans, int *rows, int *cols, double *alpha, double *matVec, int *LDA, double *vecX, int *incx, double *beta, double *vecO, int *incy);
void dtrmv_(char *UPLO, char *Trans, char *Diag, int *rows, double *matVec, int *LDA, double *vecO, int *incx);
void dger_(int *rows, int *cols, double *alpha, double *vecX, int *incx, double *vecY, int *incy, double *matOVec, int *LDA);
void dsyr_(char *UPLO, int *dimension, double *alpha, double *vec, int *incx, double *matOVec, int *LDA);
void dsyr2_(char *UPLO, int *dimension, double *alpha, double *vecX, int *incx, double *vecY, int *incy, double *matOVec, int *LDA);
void dgemm_(char *TransA, char *TransB, int *M, int *N, int *K, double *alpha, double *matVecA, int *LDA, double *matVecB, int *LDB, double *beta, double *matOVec, int *LDC);
void dsyrk_(char *UPLO, char *Trans, int *N, int *K, double *alpha, double *matVecA, int *LDA, double *beta, double *matOVec, int *LDC);
void dsyr2k_(char *UPLO, char *Trans, int *N, int *K, double *alpha, double *matVecA, int *LDA, double *matVecB, int *LDB, double *beta, double *matOVec, int *LDC);

/* Level-1 BLAS functions */
void swapVectors (vectorD *vectorX, vectorD *vectorY);
void copyVectors (vectorD *vectorX, vectorD *vectorY);
void copyMatrix (matrixD *matrix1, matrixD *matrix2);
void scaleVector (vectorD *vector, double alpha);
void scaleMatrix (matrixD *inputMat, double alpha);

void vectorAddOrig (double alpha, vectorD *vectorX, double beta, vectorD *vectorY);
void vectorAdd ( double alpha, vectorD *vectorX, double beta, vectorD *vectorY, vectorD *vecOutput);

double vectorDotProd (vectorD *vectorX, vectorD *vectorY);
double vectorNorm2 (vectorD *vector);
double vectorNorm1 (vectorD *vector);
int vectorNormInfIndex (vectorD *vector);
void vectorNormalize2 (vectorD *vector);
void matrixVectorNorm2 (matrixD *inputMat, vectorD *normValue);
void matrixNormalize2 (matrixD *inputMat);

/* Level-2 BLAS functions */
void vectorMatrixMultOrig (char Trans, double alpha, matrixD *matrix, vectorD *vectorX, double beta, vectorD *vectorY);
void vectorMatrixMult (char Trans, double alpha, matrixD *matrix, vectorD *vectorX, double beta, vectorD *vectorY, vectorD *outputVec);
void vectorTriagMatrixMult ( char UPLO, char Trans, char Diag, matrixD *inputMat, vectorD *vector, vectorD *vecOutput);

void matrixRankOne ( double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat, matrixD *outputMat);
void matrixRankOneOrig ( double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat);
void matrixSymRankOne ( char UPLO, double alpha, vectorD *inputVec, matrixD *inputMat, matrixD *outputMat);
void matrixSymRankTwo ( char UPLO, double alpha, vectorD *vectorX, vectorD *vectorY, matrixD *inputMat, matrixD *outputMat);

/* Level-3 BLAS functions */
void matrixMultiply (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, matrixD *outputMat);
void matrixMultiplyABPCOrig (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC);
void matrixMultiplyABPC (char TransA, char TransB, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC, matrixD *outputMat);

void matrixSymmRankKOrig (char UPLO, char Trans, double alpha, matrixD *matrixA, double beta, matrixD *matrixC);
void matrixSymmRankK (char UPLO, char Trans, double alpha, matrixD *matrixA, double beta, matrixD *matrixC, matrixD *outputMat);
void matrixSymmRank2K (char UPLO, char Trans, double alpha, matrixD *matrixA, matrixD *matrixB, double beta, matrixD *matrixC, matrixD *outputMat);

/*Extra Functions added by User */
void matrixHadamardProd (matrixD *matrixA, matrixD *matrixB, matrixD *outputMat);
void matrixAddition (double alpha, matrixD *matrixA, double beta, matrixD *matrixB, matrixD *outputMat);
void matrixAdditionOrig (double alpha, matrixD *matrixA, double beta, matrixD *matrixB);
double matrixQuad (vectorD *vecA, char Trans, matrixD *matA, vectorD *vecB);
#endif
