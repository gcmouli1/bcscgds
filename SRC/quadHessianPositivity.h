/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _QUADHESSIANPOSITIVITY_H_
#define _QUADHESSIANPOSITIVITY_H_
#include "quadInterpolate.h"
void quadHessianPositivity (madsStructure *mads, vectorD *optimum);
void projection (vectorD *point, madsStructure *mads, vectorD *pointProjection);
void bracketingQuad (vectorD *startPt, vectorD *direction, vectorD *midPt, vectorD *endPt, double *stepLength, double *fstartPtOrig, double *fendPtOrig, madsStructure *mads);
void lineSearchQuad (vectorD *direction, vectorD *startPt, vectorD *optPt, vectorD *endPt, double oldStepLength, madsStructure *mads);
double unConstraintedOptimQuad (vectorD *startPt, madsStructure *mads);
#endif

