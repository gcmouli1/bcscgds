/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "Halton.h"
/*
void primeGeneration (int num, int *primeCollect)
{
	int i,j;
	bool isPrime=true;

	i=2;
	int k=0;
	while (k<num)
	{
		j=2;
		isPrime=true;
		while (j <= i/2 && isPrime==true)
		{
			if (i%j==0)
			{
				isPrime=false;
			}
			j++;
		}
		if (isPrime==true)
		{
			primeCollect[k]=i;
			k++;
		}
		i++;
	}
}
*/

double haltonFunc (int index, int base)
{
	double f=1;
	double r=0;
	while (index > 0)
	{
		f=f/base;
		r=r+f*(index % base);
		index=index/base;
	}
	return r;
}

void haltonSeqGen (madsStructure *mads)
{
	int dimension=mads->dimension;
	int *primeCollect=malloc(sizeof(int)*dimension);
	primeGeneration (dimension, primeCollect,2);
//	mads->haltonSequence=malloc(sizeof(matrixD));
//	matrixInit (mads->haltonSequence, dimension, numPollPts);

	int i,j,k;
	int offset=dimension;
	//int offset=0;
	k=0;
	for (i=0; i<mads->centroidCollect->cols; i++)
	{
		for (j=0; j<dimension; j++)
		{
			//mads->haltonSequence->mat[k]=haltonFunc (i+offset, primeCollect[j]);
			mads->centroidCollect->mat[k]=haltonFunc (i+offset, primeCollect[j]);
			k++;
		}
	}
	matrixNormalize2 (mads->centroidCollect);

//	// printf("printing halton sequence\n");
//	printMatrix (haltonSequence);
	free (primeCollect);
}
