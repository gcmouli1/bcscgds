/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "simplexGradient.h"

void simplexGradInit (madsStructure *mads, simplexGradStruct *sg, bool quickSG)
{
	sg->gradDescent=0;
	sg->numVicinityPts=0;
	sg->quickSG=quickSG;
	sg->smCenterDist=malloc(sizeof(vectorD));
	vectorInit (sg->smCenterDist, mads->searchMatrix->cols);
	sg->outgoingVector=malloc(sizeof(vectorD));
	vectorInit (sg->outgoingVector, mads->dimension);
	sg->qsPollDirectionIndex=malloc(sizeof(vectorZ));
	vectorInitZ(sg->qsPollDirectionIndex, mads->searchMatrix->cols+1);
	sg->vicinityPointsIndex=malloc(sizeof(vectorZ));
	vectorInitZ (sg->vicinityPointsIndex, 2*mads->iterLimit);
	sg->vecOnesSM=malloc (sizeof(vectorD));
	vecOnesD (sg->vecOnesSM, mads->searchMatrix->cols);
	sg->tmpGrad=malloc(sizeof(vectorD));
	vectorInit (sg->tmpGrad, mads->dimension);
}

/* Gives the indices of points lying within a sphere around current point */
void vicinityPointsEval (madsStructure *mads, char searchParameter, simplexGradStruct *sg)
{
	int i, cols;
	for (i=0; i<mads->numFeasibleElem; i++)
	{
		sg->vicinityPointsIndex->vec[i] = -1;
	}


	//vectorMatrixMultOrig ('T', 1.0, mads->ptSet, mads->center, 0.0, mads->vTcFull);
//	printf ("printing numFeasibleElem=%d and dimension=%d\n",mads->numFeasibleElem,mads->dimension);
	vectorD *vTc=malloc(sizeof(vectorD));
	vectorInit (vTc, mads->numFeasibleElem);
	matrixD *ptSetTmp=malloc (sizeof(matrixD));
	matrixInit (ptSetTmp, mads->dimension, mads->numFeasibleElem);

	memcpy (ptSetTmp->mat, mads->ptSet->mat, sizeof(double)*(mads->numFeasibleElem)*(mads->dimension));
//	printf ("printing center in vicinityfunc in simplexgrad\n");
//	printVector (mads->center);
	// printf("printing ptSetTmp\n");
//	printMatrix (ptSetTmp);
	vectorMatrixMultOrig ('T', 1.0, ptSetTmp, mads->center, 0.0, vTc);
	freeMatrixD (ptSetTmp);
	//for (i=0; i<vTc->dimension; i++)
	//{
	//	vTc->vec[i]=mads->vTcFull->vec[i];
	//	vTc->vec[i]=mads->vTcTmp->vec[i];
	//}
//	double epsilon=1E-5;
	double radiusSquare=pow((mads->radius*1.01),2);
	double ptDistance;
	sg->numVicinityPts=0;
	//// printf ("printing ptSquare elements\n");
	//printVector (mads->ptSquare);
	// printf ("centerIndex is %d\n",mads->centerIndex);
	//// printf ("vTc vector \n");
	//printVector (vTc);
	if (searchParameter == 'F')
	{
		for (cols=0; cols<mads->numFeasibleElem; cols++)
		{
		//	printf ("%lf\t%lf\t%lf\n",mads->ptSquare->vec[cols],mads->ptSquare->vec[mads->centerIndex],vTc->vec[cols]);

			ptDistance=mads->ptSquare->vec[cols]+mads->ptSquare->vec[mads->centerIndex]-2*vTc->vec[cols];
		//	printf ("ptDistance[%d]=%f\n",cols,ptDistance);
			if (ptDistance <= radiusSquare && cols != mads->centerIndex && mads->pointType->vec[cols]==0)
			{
				sg->vicinityPointsIndex->vec[sg->numVicinityPts]=cols;
				sg->numVicinityPts++;
			}
		}
		if (sg->numVicinityPts < mads->searchMatrix->cols)
		{
			for (cols=0; cols <sg->numVicinityPts; cols++)
			{
				sg->vicinityPointsIndex->vec[cols]=-1;
			}
			sg->numVicinityPts=0;

			for (cols=0; cols<mads->searchMatrix->cols; cols++)
			{
				sg->vicinityPointsIndex->vec[sg->numVicinityPts]=mads->funcIndex[cols];
				sg->numVicinityPts++;
			}
		}
	}
	else
	{
		for (cols=0; cols<mads->searchMatrix->cols; cols++)
		{
			ptDistance=mads->ptSquare->vec[mads->funcIndex[cols]]+mads->ptSquare->vec[mads->centerIndex]-2*vTc->vec[mads->funcIndex[cols]];
			// printf ("ptDistance[%d]=%f\n", cols, ptDistance);
			if (ptDistance <= radiusSquare && mads->funcIndex[cols] != mads->centerIndex) 
			{
				sg->vicinityPointsIndex->vec[sg->numVicinityPts]=mads->funcIndex[cols];
				sg->numVicinityPts++;
			}
		}

		freeVectorD (vTc);
	}
}

//void quickSimplexGradient (madsStructure *mads, simplexGradStruct *sg)
//{
//	int cols;
//	int i,j,k;
//	vectorD *smTc=malloc (sizeof(vectorD));
//	vectorInit (smTc, mads->searchMatrix->cols);
//	vectorMatrixMultOrig ('T', 1.0, mads->searchMatrix, mads->center, 0.0, smTc);
//	double epsilon=1E-5;
//	double radiusSquare=(mads->radius)*(mads->radius)+epsilon*mads->radius;
//	double maxSmCenterDist=-1;
//	double minSmCenterDist=INFS;
//	int maxSmCenterDistIndex=-1;
//	int minSmCenterDistIndex=-1;
//	//// printf("mads centerIndex=%d\n",mads->centerIndex);
//	//	// printf ("printing searchmatrix\n");
//	//	printMatrix (mads->searchMatrix);
//	for (cols=0; cols<mads->searchMatrix->cols; cols++)
//	{
//		sg->smCenterDist->vec[cols]=mads->ptSquare->vec[mads->funcIndex[cols]]+mads->ptSquare->vec[mads->centerIndex]-2*smTc->vec[cols];
//		if (maxSmCenterDist <= sg->smCenterDist->vec[cols])
//		{
//			maxSmCenterDist = sg->smCenterDist->vec[cols];
//			maxSmCenterDistIndex=cols;
//		}
//		if (minSmCenterDist >= sg->smCenterDist->vec[cols])
//		{
//			minSmCenterDist = sg->smCenterDist->vec[cols];
//			minSmCenterDistIndex=cols;
//		}
//		//		// printf ("smCenterDist[%d]=%f\n",cols,mads->smCenterDist->vec[cols]);
//	}
//	double qsDiameter=sqrt(maxSmCenterDist)+sqrt(minSmCenterDist);
//	// printf ("max distance index is %d\n",maxSmCenterDistIndex);
//
//	matrixD *vicinityPoints = malloc (sizeof(matrixD));
//	matrixInit (vicinityPoints, mads->dimension, mads->searchMatrix->cols-1);
//
//	for (cols=0; cols < maxSmCenterDistIndex; cols++)
//	{
//		sg->qsPollDirectionIndex->vec[cols]=mads->funcIndex[cols];
//		memcpy (vicinityPoints->mat+(mads->dimension)*cols, mads->ptSet->mat+(mads->dimension*(mads->funcIndex[cols])), mads->dimension*sizeof(double));
//	}
//
//	if(maxSmCenterDistIndex != mads->searchMatrix->cols-1)
//	{
//		for (cols = maxSmCenterDistIndex+1; cols < mads->searchMatrix->cols; cols++)
//		{
//			sg->qsPollDirectionIndex->vec[cols-1]=mads->funcIndex[cols];
//			memcpy (vicinityPoints->mat+(mads->dimension)*(cols-1), mads->ptSet->mat+(mads->dimension*(mads->funcIndex[cols])), mads->dimension*sizeof(double));
//		}
//	}
//	int vicinitySMSize=mads->searchMatrix->cols-1;
//	//	// printf("printing qsPollDirectionIndex\n");
//	//	for (i=0; i<vicinitySMSize; i++)
//	//	{
//	//		// printf("%d\t",mads->qsPollDirectionIndex[i]);
//	//	}
//	//	// printf("\n");
//	//	// printf("printing vicinityPoints matrix at first in QS \n");
//	//	printMatrix (vicinityPoints);
//	matrixD *vicinityPointsScaled=malloc (sizeof(matrixD));
//	matrixInitFromMatrix (vicinityPointsScaled, vicinityPoints);
//	copyMatrix (vicinityPoints, vicinityPointsScaled);
//	vectorD *vecOnesVicinity1=malloc (sizeof(vectorD));
//	vecOnesD (vecOnesVicinity1, vicinityPoints->cols);
//
//	matrixRankOneOrig (-1.0, mads->center, vecOnesVicinity1, vicinityPointsScaled);
//	freeVectorD (vecOnesVicinity1);
//	//	// printf("printing vicinityPoints scaled \n");
//	//	printMatrix (vicinityPointsScaled);
//
//
//	/* Compute whether vicinityPoints is poised or not by computing rank of vicinityPoints matrix */
//	/* Compute SVD or QR decomposition of vicinity points */
//
//	// printf("starting SVD decomposition\n");
//	matrixD *U=malloc (sizeof(matrixD));
//	matrixInit (U, vicinityPoints->rows, vicinityPoints->rows);
//	vectorD *S=malloc (sizeof(vectorD));
//	int rowColMin;
//	if (vicinityPoints->rows < vicinityPoints->cols)
//	{
//		rowColMin=vicinityPoints->rows;
//	}
//	else
//	{
//		rowColMin=vicinityPoints->cols;
//	}
//	vectorInit (S, rowColMin);
//	matrixD *VT=malloc (sizeof(matrixD));
//	matrixInit (VT, vicinityPoints->cols, vicinityPoints->cols);
//	matrixSVDD (vicinityPointsScaled, U, S, VT);
//
//	double rank=0;
//	for (i=0; i<rowColMin; i++)
//	{
//		if (S->vec[i] > mads->svdTolerance)
//		{
//			rank++;
//		}
//	}
//	double det=1;
//	for (i=0; i<rowColMin; i++)
//	{
//		det=det*S->vec[i];
//	}
//	det=fabs(det);
//
//	//	// printf ("printing rank of vicinity pts matrix=%f\n",rank);
//	//	// printf ("printing determinant of vicinity pts matrix=%f\n",det);
//	//	// printf("printing vicinity point-center in quick simplex before replacement\n");
//	//	printMatrix (vicinityPointsScaled);
//
//	/* If new point lies in subspace of n vectors */
//	//incomingVectorIndex gives index from ptSet whereas outgoingVectorIndex gives index from searchMatrix
//	int incomingVectorIndex, outgoingVectorIndex;
//	double psvValue;
//	if (det < mads->svdTolerance && S->vec[S->dimension-1] < mads->svdTolerance)
//	{
//		vicinitySMSize=mads->searchMatrix->cols;
//		// printf("Needs replacement of one of the vectors\n");
//		if (rank < mads->dimension-1)
//		{
//			// printf ("Serious trouble in quick simplex gradient\n");
//			exit(10);
//		}
//		matrixD *V=malloc (sizeof(matrixD));
//		matrixInitFromMatrix (V, VT);
//		matrixTranspose (VT, V);
//		vectorD *nullSpaceVector = malloc (sizeof(vectorD));
//		vectorInit (nullSpaceVector, mads->dimension);
//		bool nullSpace=false;
//		i=0;
//		while (i<rowColMin && nullSpace == false)
//		{
//			if (fabs(S->vec[i]) < mads->svdTolerance)
//			{
//				vecExtractMat (nullSpaceVector, V, i);
//				nullSpace = true;
//			}
//			i++;
//		}
//
//		//		// printf("printing U matrix\n");
//		//		printMatrix (U);
//		//// printf("printing S vector\n");
//		//printVector (S);
//		//exit (10);
//		//		// printf("printing VT matrix\n");
//		//		printMatrix (VT);
//		//		// printf("printing V matrix\n");
//		//		printMatrix (V);
//
//		i=0;
//		bool outgoingVector=false;
//		outgoingVectorIndex=-1;
//		//		// printf ("nullSpaceVector is\n");
//		//		printVector (nullSpaceVector);
//		while (i < mads->dimension && outgoingVector == false)
//		{
//			if (fabs (nullSpaceVector->vec[i]) > mads->svdTolerance)
//			{
//				outgoingVectorIndex=i;
//				outgoingVector == true;
//			}
//			i++;
//		}
//
//		/* Compute a vector to make the system minimum positive spanning set */
//		int rows=vicinityPoints->rows;
//		int cols=vicinityPoints->cols;
//		incomingVectorIndex=mads->funcIndex[maxSmCenterDistIndex];
//		// printf ("outgoing vector index=%d and incoming vector index=%d\n",outgoingVectorIndex, incomingVectorIndex);
//		vecExtractMat (sg->outgoingVector, vicinityPoints, outgoingVectorIndex);
//
//		memcpy (vicinityPoints->mat+rows*outgoingVectorIndex, mads->ptSet->mat+rows*incomingVectorIndex, sizeof(double)*rows);
//		vectorD *vecOnesVicinity=malloc(sizeof(vectorD));
//		vecOnesD (vecOnesVicinity, vicinityPoints->cols);
//		vectorMatrixMultOrig ('N', -1.0, vicinityPoints, vecOnesVicinity, 0.0, mads->tmpVec);
//		//		// printf("printing positive spanning vector unscaled\n");
//		//		printVector (mads->tmpVec);
//		vectorNormalize2 (mads->tmpVec);
//		scaleVector (mads->tmpVec, qsDiameter/2);
//		psvValue=singlePtEval (mads->tmpVec, mads);
//		//		// printf("printing vicinityPoints inside det < 0 case\n");
//		//		printMatrix (vicinityPoints);
//		//		// printf("printing positive spanning vector \n");
//		//		printVector (mads->tmpVec);
//		matrixVectorAppend ('B', vicinityPoints, mads->tmpVec);
//		matrixVectorAppend ('B', vicinityPoints, sg->outgoingVector);
//		freeMatrixD (V);
//		freeVectorD (vecOnesVicinity);
//		//		// printf("printing qsPollDirectionIndex\n");
//		//		for (i=0; i<vicinitySMSize+1; i++)
//		//		{
//		//			// printf("%d\t",mads->qsPollDirectionIndex[i]);
//		//		}
//		//		// printf("\n");
//		sg->qsPollDirectionIndex->vec[vicinitySMSize]=sg->qsPollDirectionIndex->vec[outgoingVectorIndex];
//		sg->qsPollDirectionIndex->vec[outgoingVectorIndex]=incomingVectorIndex;
//		//		// printf("printing qsPollDirectionIndex\n");
//		//		for (i=0; i<vicinitySMSize+1; i++)
//		//		{
//		//			// printf("%d\t",mads->qsPollDirectionIndex[i]);
//		//		}
//		//		// printf("\n");
//		vicinitySMSize++; //to account for removed vector being retained and new pss completion vector
//	}
//
//	//	// printf("printing vicinity point matrix in quick simplex after replacement\n");
//	//	printMatrix (vicinityPoints);
//
//	//	// printf ("printing funcIndex of searchMatrix from previous step\n");
//	//	for (i=0; i<mads->searchMatrix->cols; i++)
//	//	{
//	//		// printf("%d\t",mads->funcIndex[i]);
//	//	}
//	//	// printf("\n");
//
//	vicinityPointsEval (mads, 'F', sg);
//	//	// printf ("printing vicinity points index\n");
//	//	for (i=0; i<mads->numVicinityPts; i++)
//	//	{
//	//		// printf("%d\t",mads->vicinityPointsIndex[i]);
//	//	}
//	//	// printf("\n");
//	int numNonPollPts=0;
//	int *nonPollPtsIndex=malloc (sizeof(int)*sg->numVicinityPts);
//	bool indexRepeat;
//	for(i=0; i<sg->numVicinityPts; i++)
//	{
//		indexRepeat=false;
//		j=0;
//		while (j < mads->searchMatrix->cols && indexRepeat==false)
//		{
//			if (sg->vicinityPointsIndex->vec[i] == mads->funcIndex[j])
//			{
//				indexRepeat=true;
//			}
//			j++;
//		}
//		if (indexRepeat==false)
//		{
//			nonPollPtsIndex[numNonPollPts]=sg->vicinityPointsIndex->vec[i];
//			numNonPollPts++;
//		}
//	}
//	// printf ("printing number of nonpoll points=%d\n",numNonPollPts);
//	//	// printf ("printing non poll step indices\n");
//	//	for (i=0; i<numNonPollPts; i++)
//	//	{
//	//		// printf("%d\t",nonPollPtsIndex[i]);
//	//	}
//	//	// printf("\n");
//
//	//k=mads->searchMatrix->cols-1;
//	k=vicinitySMSize;
//	//// printf ("printing qsPollDirectionIndex \n");
//	//printVectorZ (sg->qsPollDirectionIndex);
//	// printf ("printing value of vicinitySMSize=%d and numNonPollPts=%d\n",vicinitySMSize,numNonPollPts);
//	for (i=0; i<k; i++)
//	{
//		sg->vicinityPointsIndex->vec[i]=sg->qsPollDirectionIndex->vec[i];
//	}
//	for(i=0; i<numNonPollPts; i++)
//	{
//		sg->vicinityPointsIndex->vec[k+i]=nonPollPtsIndex[i];
//	}
//	//	// printf ("printing vicinity points index after adjustment\n");
//
//	sg->numVicinityPts=vicinitySMSize+numNonPollPts;
//	//	for (i=0; i<mads->numVicinityPts; i++)
//	//	{
//	//		// printf("%d\t",mads->vicinityPointsIndex[i]);
//	//	}
//	//	// printf("\n");
//
//	// printf("vicinitySMSize=%d and numNonPollPts=%d\n",vicinitySMSize,numNonPollPts);
//	vicinityPoints->mat=realloc (vicinityPoints->mat, sizeof(double)*(mads->dimension)*(vicinitySMSize+numNonPollPts));
//	//vicinityPoints->cols=mads->searchMatrix->cols-1+numNonPollPts;
//	vicinityPoints->cols=vicinitySMSize+numNonPollPts;
//
//	//k=mads->searchMatrix->cols-1;
//	k=vicinitySMSize;
//	bool repeatVector=false;
//	for (cols=0; cols<numNonPollPts; cols++)
//	{
//		memcpy (vicinityPoints->mat+(mads->dimension)*k, mads->ptSet->mat+(mads->dimension * nonPollPtsIndex[cols]), mads->dimension*sizeof(double));
//		k++;
//	}
//
//	//	// printf("points in vicinity of center \n");
//	//	printMatrix (vicinityPoints);
//
//	vectorD *vecOnesVicinity=malloc (sizeof(vectorD));
//	vecOnesD (vecOnesVicinity, sg->numVicinityPts);
//
//	matrixRankOneOrig (-1.0, mads->center, vecOnesVicinity, vicinityPoints);
//	freeVectorD (vecOnesVicinity);
//
//	vectorD *vicinityPointsFuncVal=malloc (sizeof(vectorD));
//	vectorInit (vicinityPointsFuncVal, sg->numVicinityPts);
//
//	for (i=0; i<sg->numVicinityPts; i++)
//	{
//		//	// printf ("func difference %i iteration=%f-%f\n",i, mads->funcSet->vec[mads->vicinityPointsIndex[i]],mads->funcSet->vec[mads->centerIndex]);
//		vicinityPointsFuncVal->vec[i]=mads->funcSet->vec[sg->vicinityPointsIndex->vec[i]]-mads->funcSet->vec[mads->centerIndex];
//	}
//	//	// printf("printing function value difference for vicinity points\n");
//	//	printVector (vicinityPointsFuncVal);
//
//	//	// printf("points-center in vicinity of center \n");
//	//	printMatrix (vicinityPoints);
//	//Following step will store Least Square solution in vicinityPointsFuncVal
//	//vicinityPoints and vicinityPointsFuncVal are overwritten
//	leastSquareOrig ('T', vicinityPoints, vicinityPointsFuncVal);
//	for (i=0; i<mads->dimension; i++)
//	{
//		mads->gradient->vec[i]=vicinityPointsFuncVal->vec[i];
//	}
//
//	/*
//	   copyVectors (mads->gradient, mads->tmpGrad);
//	   scaleVector (mads->tmpGrad, -1.0);
//	   // printf("printing negGrad \n");
//	   printVector (mads->tmpGrad);
//	   double gradFuncVal=singlePtEval (mads->tmpGrad, mads);
//	   */
//
//	freeVectorD (S);
//	freeMatrixD (U);
//	freeMatrixD (VT);
//	freeVectorD (vicinityPointsFuncVal);
//	freeVectorD (smTc);
//	//freeVectorD (leastSqSolution);
//	freeMatrixD (vicinityPoints);
//	freeMatrixD (vicinityPointsScaled);
//}

void simplexGradient (madsStructure *mads, simplexGradStruct *sg)
{
	int i; //j,k;
//	printf ("mads radius in simplex gradient beginning %f\n",mads->radius);
	//mads->centralDistance is the distance between old center and new center
	//It makes sure that quick SG is not computed in the first stage and also when new point is far from old point
//	if (sg->quickSG == true && mads->centralDistance < 0.05*mads->radius)
//	{
//		// printf("Activating quick simplex gradient\n");
//		quickSimplexGradient (mads, sg);
//		return;
//	}
//	mads->allbadPts = true;
	//	int *funcIndex=malloc (sizeof(int)*(mads->searchMatrix->cols));
	int cols=0;

//	double simplexBestVal=mads->bestVal;
	//matrixRankOne (-1.0, mads->center, sg->vecOnesSM, mads->searchMatrix, mads->searchMatrixOrigin);

	vicinityPointsEval (mads, 'F', sg);
	matrixD *vicinityPoints = malloc (sizeof(matrixD));
	matrixInit (vicinityPoints, mads->dimension, sg->numVicinityPts);

/*	
	 printf("printing vicinity points index\n");
	for (i=0; i<sg->numVicinityPts; i++)
	{
		printf("%d\t",sg->vicinityPointsIndex->vec[i]);
	}
	 printf("\n");
	
	 */

	for (cols=0; cols<sg->numVicinityPts; cols++)
	{
		memcpy (vicinityPoints->mat+(mads->dimension)*cols, mads->ptSet->mat+(mads->dimension)*(sg->vicinityPointsIndex->vec[cols]), (mads->dimension)*sizeof(double));
	}

	//// printf("points in vicinity of center \n");
	//printMatrix (vicinityPoints);

	vectorD *vecOnesVicinity=malloc (sizeof(vectorD));
	vecOnesD (vecOnesVicinity, sg->numVicinityPts);

	matrixRankOneOrig (-1.0, mads->center, vecOnesVicinity, vicinityPoints);

	vectorD *vicinityPointsFuncVal=malloc (sizeof(vectorD));
	vectorInit (vicinityPointsFuncVal, sg->numVicinityPts);

	for (i=0; i<sg->numVicinityPts; i++)
	{
		vicinityPointsFuncVal->vec[i]=mads->funcSet->vec[sg->vicinityPointsIndex->vec[i]]-mads->funcSet->vec[mads->centerIndex];
	}

//	 printf("printing function value difference for vicinity points\n");
//	printVector (vicinityPointsFuncVal);

//	 printf("points-center in vicinity of center \n");
//	printMatrix (vicinityPoints);

	//Following step will store Least Square solution in vicinityPointsFuncVal
	//vicinityPoints and vicinityPointsFuncVal are overwritten
	if (sg->numVicinityPts < mads->dimension+1)
	{
		copyVectors (mads->bestPt,mads->gradient);
		scaleVector (mads->gradient, -1.0);
	}
	else
	{
		leastSquareOrig ('T', vicinityPoints, vicinityPointsFuncVal);
		for (i=0; i<mads->dimension; i++)
		{
			mads->gradient->vec[i]=vicinityPointsFuncVal->vec[i];
		}
	}

	freeVectorD (vecOnesVicinity);
	freeVectorD (vicinityPointsFuncVal);
	freeMatrixD (vicinityPoints);
}

void negGradFuncEval (madsStructure *mads, userData *uData)
{

	copyVectors (mads->gradient, mads->negGrad);
	vectorNormalize2 (mads->negGrad);
	scaleVector (mads->negGrad, -1.0);
	vectorAddOrig (1.0, mads->center, mads->radius, mads->negGrad);
//	printf ("printing center\n");
//	printVector (mads->center);
//	printf ("simplex gradient\n");
//	printVector (mads->gradient);
//	printf ("printing neg grad\n");
//	printVector (mads->negGrad);
//	printf ("computing negative of simplex gradient\n");
	mads->negGradFuncValue=singlePtEval (mads->negGrad, mads, uData);
}

void freeSimplexGrad (simplexGradStruct *sg)
{
	freeVectorD (sg->smCenterDist);
	freeVectorD (sg->tmpGrad);
	freeVectorD (sg->outgoingVector);
	freeVectorZ (sg->qsPollDirectionIndex);
	freeVectorZ (sg->vicinityPointsIndex);
	freeVectorD (sg->vecOnesSM);
	free (sg);
} 
