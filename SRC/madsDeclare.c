/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "madsDeclare.h"

//void madsInit (madsStructure *mads, vectorD *startPt, int dimension, double radius, double precision, int iterLimit, int numPollSteps, vectorD *varLB, vectorD *varUB, int numConstr)
void madsInit (madsStructure *mads, vectorD *startPt, int dimension, double radius, double precision, int iterLimit, int numPollSteps, double *varLB, double *varUB, int numConstr, double boxLengthMin, char objectiveType)
{
	int i,j;
	//mads->funcList=origFuncList;
	//fclose(mads->funcList);
	mads->numPollSteps=numPollSteps;
	mads->pollStepLimitCrossed=false;
	mads->penaltyParam=10;
	mads->numConstr=numConstr;
	mads->boxRadius=boxLengthMin;
	mads->radiusRebootPrecision=1E-10;
	mads->diffLSbestValuePrecision=1E-10;
	mads->objectiveType=objectiveType;
	if (objectiveType == 'O')
	{
		mads->funcList=fopen("OUTPUTLIST.txt","w");
	}
	if (dimension <3)
	{
		mads->numVicinityPtsMax=2;
	}
	else if (dimension <20)
	{
		mads->numVicinityPtsMax=4;
	}
	else
	{
		mads->numVicinityPtsMax=0.1*dimension;
	}
	mads->ptIndex=-1;
	mads->ptInfeasible=false;
	mads->ptSetHardFeasibility=malloc (sizeof(bool)*20*iterLimit);
	mads->ptSetSoftFeasibility=malloc (sizeof(char)*20*iterLimit);
	for (i=0; i<20*iterLimit; i++)
	{
		mads->ptSetHardFeasibility[i]=true;
		mads->ptSetSoftFeasibility[i]='U';
	}
	mads->radiusLimit=precision;
	mads->numElem=0;
	mads->numFeasibleElem=0;
	mads->numInfeasibleElem=0;
	mads->radius=radius;
	mads->radiusInit=radius;
	mads->dimension=dimension;
	mads->svdTolerance=1E-10;
	mads->bestVal=INFS;
	mads->bestPtIndex=-1;
	mads->centerIndex=0;
	mads->iterLimit=iterLimit;
	mads->iterLimitActual=iterLimit;
	mads->centralDistance=INFS;
	mads->negGradFuncValue=INFS; 
	mads->gradDescent=false;
	mads->bestPt=malloc (sizeof(vectorD));
	vectorInit (mads->bestPt, dimension);
	copyVectors (startPt, mads->bestPt);
	mads->center=malloc (sizeof(vectorD));
	vectorInit (mads->center, dimension);
	copyVectors (startPt, mads->center);
	mads->ptSet=malloc (sizeof(matrixD));
	matrixInit (mads->ptSet, dimension, 2*iterLimit);
	for (i=0; i<dimension*2*iterLimit; i++)
	{
		mads->ptSet->mat[i]=1e15;
	}
	mads->funcSet=malloc (sizeof(vectorD));
	vectorInit (mads->funcSet, 2*iterLimit);
	mads->funcSetActual=malloc (sizeof(vectorD));
	vectorInit (mads->funcSetActual, 2*iterLimit);
	mads->constrSet=malloc (sizeof(vectorD));
	vectorInit (mads->constrSet, 2*iterLimit);

	for (i=0; i<2*iterLimit; i++)
	{
		mads->funcSet->vec[i]=1e15;
		mads->funcSetActual->vec[i]=1e15;
	}

	mads->ptSquare=malloc (sizeof(vectorD));
	vectorInit (mads->ptSquare, 2*iterLimit);
	mads->pointType=malloc (sizeof(vectorZ));
	vecZerosZ (mads->pointType, 2*iterLimit);
	mads->numLineSearch=0;
	mads->lineSearchPoint=false;
	mads->madsRadius=1;
	mads->iterLimitCrossed=0;
	mads->vecOnes=malloc (sizeof(vectorD));
	vecOnesD (mads->vecOnes, dimension);
	mads->vecOnesNP1=malloc (sizeof(vectorD));
	vecOnesD (mads->vecOnesNP1, dimension+1);
	mads->normSpanVec=malloc (sizeof(matrixD));
	matOnesD (mads->normSpanVec, dimension, dimension);  //later expanded to contain n+1 th direction
	mads->searchMatrix=malloc (sizeof(matrixD));
	matOnesD (mads->searchMatrix, dimension, dimension+1); 
	mads->searchMatrixOrigin=malloc (sizeof(matrixD));
	matOnesD (mads->searchMatrixOrigin, dimension, dimension+1); 
	mads->funcIndex=malloc (sizeof(int)*mads->searchMatrix->cols);
	mads->vecOnesSM=malloc (sizeof(vectorD));
	vecOnesD (mads->vecOnesSM, mads->searchMatrix->cols);
	mads->madsRecFactor1=4;
	//mads->madsRecFactor2=pow(4,20);
	mads->madsRecFactor2=4;
	mads->meshLenRedFactor=pow (4,10);
	mads->vecHash1=malloc (sizeof(vectorD));
	vectorInit (mads->vecHash1, dimension);
	mads->vecHash2=malloc (sizeof(vectorD));
	vectorInit (mads->vecHash2, dimension);
	mads->vecHash3=malloc (sizeof(vectorD));
	vectorInit (mads->vecHash3, dimension);
	mads->ptSetHash1=malloc (sizeof(vectorD));
	vectorInit (mads->ptSetHash1, 20*iterLimit);
	mads->ptSetHash2=malloc (sizeof(vectorD));
	vectorInit (mads->ptSetHash2, 20*iterLimit);
	mads->ptSetHash3=malloc (sizeof(vectorD));
	vectorInit (mads->ptSetHash3, 20*iterLimit);
	mads->funcVector=malloc (sizeof(vectorD));
	vectorInit (mads->funcVector,3);
	mads->singlePointMatrix=malloc (sizeof(matrixD));
	matrixInit (mads->singlePointMatrix, dimension, 1);
	mads->tmpVec=malloc (sizeof(vectorD));
	vectorInit (mads->tmpVec, dimension);
	mads->tmpPt=malloc (sizeof(double)*dimension);
	mads->rotatePt=malloc (sizeof(vectorD));
	vectorInit (mads->rotatePt, dimension);
	mads->allbadPts=false;
	mads->allWorsePts=false;
	mads->badDistribution=false;
	mads->gradient=malloc(sizeof(vectorD));
	vectorInit (mads->gradient, dimension);
	mads->negGrad=malloc(sizeof(vectorD));
	vectorInit (mads->negGrad, dimension);
	mads->gradDescent=false;
	mads->vTcFull=malloc(sizeof(vectorD));
	vectorInit (mads->vTcFull, 2*iterLimit);
	mads->simplexFuncValues=malloc(sizeof(vectorD));
	vectorInit (mads->simplexFuncValues, mads->searchMatrix->cols);
	mads->centroidCollect=malloc (sizeof(matrixD));
	matrixInit (mads->centroidCollect, dimension, numPollSteps);


	/* Computing normSpanVec, a matrix of equiangular vectors */
	/* Setting all elements to -1/n except diagonal ones */
	scaleMatrix (mads->normSpanVec, -1.0/dimension);
	int rows=mads->normSpanVec->rows;
	for (i=0; i<dimension; i++)
	{
		mads->normSpanVec->mat[i+i*rows]=1;
	}

	/* Computing Cholesky decomposition */
	matrixCholeskyDecOrig ('U', mads->normSpanVec);
	for (i=0; i<dimension-1; i++)
	{
		for (j=i+1; j<dimension; j++)
		{
			mads->normSpanVec->mat[j+i*rows]=0;
		}
	}

	//vectorD *tmp=malloc (sizeof(vectorD));
	//vectorInit (tmp, dimension);
	vectorMatrixMultOrig ('N', -1.0, mads->normSpanVec, mads->vecOnes, 0.0, mads->tmpVec);
	vectorNormalize2 (mads->tmpVec);
	matrixVectorAppend ('B', mads->normSpanVec, mads->tmpVec);
	//freeVectorD (tmp);

	/* Generating random number for hash vectors */
//	srand(time(0));
//	int sizeRandom=iterLimit;
//	if (iterLimit > RAND_MAX)
//	{
//		sizeRandom=RAND_MAX/(dimension+10);
//	}
//
//	for (i=0; i<dimension; i++)
//	{
//		mads->vecHash1->vec[i]=rand()%sizeRandom+100;
//	}
//
//	for (i=0; i<dimension; i++)
//	{
//		mads->vecHash2->vec[i]=rand()%sizeRandom+100;
//	}
//
//	for (i=0; i<dimension; i++)
//	{
//		mads->vecHash3->vec[i]=rand()%sizeRandom+100;
//	}


	int numVecHashes=3;
	int *primeCollect = malloc (sizeof(int)*dimension);
	double *primeTwisted = malloc (sizeof(double)*dimension*numVecHashes);
	int offset=100;
	primeGeneration (dimension, primeCollect, offset);
	for (j=0; j<3; j++)
	{
		for (i=0; i<dimension; i++)
		{
			primeTwisted[i+j*dimension] = pow (primeCollect[i],j+1);
		}
	}
	memcpy (mads->vecHash1->vec, primeTwisted, sizeof(double)*dimension);
	memcpy (mads->vecHash2->vec, primeTwisted+dimension, sizeof(double)*dimension);
	memcpy (mads->vecHash3->vec, primeTwisted+2*dimension, sizeof(double)*dimension);
//	// printf ("printing vecHash1\n");
//	printVector (mads->vecHash1);
//	// printf ("printing vecHash2\n");
//	printVector (mads->vecHash2);
//	// printf ("printing vecHash3\n");
//	printVector (mads->vecHash3);
	free (primeCollect);
	free (primeTwisted);


	mads->varLB=malloc (sizeof(vectorD));
	vectorInit (mads->varLB, dimension);
	//copyVectors (varLB, mads->varLB);
	memcpy (mads->varLB->vec, varLB, sizeof(double)*dimension);
	mads->varUB=malloc (sizeof(vectorD));
	vectorInit (mads->varUB, dimension);
	//copyVectors (varUB, mads->varUB);
	memcpy (mads->varUB->vec, varUB, sizeof(double)*dimension);

	mads->v1=malloc (sizeof(vectorD));
	vectorInit (mads->v1, dimension);
	mads->v2=malloc (sizeof(vectorD));
	vectorInit (mads->v2, dimension);

	mads->constrVec=malloc (sizeof(double)*numConstr);
	mads->exactPenaltyConstr=malloc (sizeof(vectorD));
	vectorInit (mads->exactPenaltyConstr, numConstr);

	mads->numEllipsoids=0.1*iterLimit/(dimension+1);
	mads->ellipsoidRadiusMatrix=malloc (sizeof(matrixD));
	matrixInit (mads->ellipsoidRadiusMatrix, dimension, mads->numEllipsoids);
	mads->ellipsoidScale=malloc (sizeof(vectorD));
	vectorInit (mads->ellipsoidScale, dimension+1);

	mads->ptsInterpolate=malloc (sizeof(matrixD));
	matrixInitFromMatrix (mads->ptsInterpolate, mads->ptSet);
	mads->interpolateFuncVal=malloc (sizeof(vectorD));
	vectorInit (mads->interpolateFuncVal, mads->funcSet->dimension);
	mads->funcEvalPtsTmp=malloc (sizeof(vectorD));
	vectorInit (mads->funcEvalPtsTmp, mads->searchMatrix->cols);

	mads->numIterpolatePts=0;
	mads->numIneqConstrQuad=2*dimension;
	mads->numEqConstrQuad=0;

	mads->quadAlphaL=malloc (sizeof(vectorD));
	vectorInit (mads->quadAlphaL, dimension+1);
	mads->quadAlphaQ=malloc (sizeof(vectorD));
	vectorInit (mads->quadAlphaQ, dimension*(dimension+1)/2);
	mads->quadFitPSD=true;
	mads->quadHessian=malloc (sizeof(matrixD));
	matrixInit (mads->quadHessian, dimension, dimension);
	mads->quadLinear=malloc (sizeof(vectorD));
	vectorInit (mads->quadLinear, dimension);
	mads->direction=malloc (sizeof(vectorD));
	vectorInit (mads->direction, dimension);
}

void freeMads(madsStructure *mads)
{
	freeVectorD (mads->bestPt);
	freeVectorD (mads->center);
	freeMatrixD (mads->ptSet);
	freeVectorD (mads->funcSet);
	freeVectorD (mads->funcSetActual);
	freeVectorD (mads->vecOnes);
	freeVectorD (mads->vecOnesNP1);
	freeMatrixD (mads->normSpanVec);
	freeVectorD (mads->vecHash1);
	freeVectorD (mads->vecHash2); 
	freeVectorD (mads->vecHash3); 
	freeVectorD (mads->ptSetHash1); 
	freeVectorD (mads->ptSetHash2);
	freeMatrixD (mads->singlePointMatrix);
	freeVectorD (mads->funcVector);
	freeVectorD (mads->tmpVec);
	freeVectorD (mads->varLB);
	freeVectorD (mads->varUB);
	freeVectorD (mads->v1);
	freeVectorD (mads->v2);
	freeMatrixD (mads->ellipsoidRadiusMatrix);
	freeVectorD (mads->ellipsoidScale);
	freeMatrixD (mads->ptsInterpolate);
	freeVectorD (mads->interpolateFuncVal);
	freeVectorD (mads->funcEvalPtsTmp);
	freeVectorD (mads->quadAlphaL);
	freeVectorD (mads->quadAlphaQ);
	freeMatrixD (mads->quadHessian);
	freeVectorD (mads->quadLinear);
	freeVectorD (mads->direction);
	free (mads->constrVec);
	free (mads->tmpPt);
	free (mads->ptSetSoftFeasibility);
	free (mads->ptSetHardFeasibility);
	free (mads);
}

