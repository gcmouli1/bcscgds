/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "singlePtEval.h"

double singlePtEval (vectorD *point, madsStructure *mads, userData *uData)
{
	vectorMatrixConvert ('M', point, mads->singlePointMatrix);
	int *inputUniqueRepeatIndex = malloc(sizeof(int)*1);
	mads->allbadPts=0;
	double funcVal = -INFS;
	rmDuplicate (mads->singlePointMatrix, inputUniqueRepeatIndex, mads, uData);
	if (mads->iterLimitCrossed==true)
	{
		// printf ("iterlimit crossed in singlePtEval\n");
		free (inputUniqueRepeatIndex);
		return mads->bestVal;
	}
	else
	{
		int a=isfinite(mads->funcSet->vec[inputUniqueRepeatIndex[0]]);
		if (a == 0)
		{
			// printf("vector causing NaN trouble\n");
		//	printVector(point);
			exit(10);
		}


		if(inputUniqueRepeatIndex[0] > mads->numFeasibleElem-1)
		{
			funcVal=1E20;
		//	int i=0;
		//	for (i=0; i<mads->dimension; i++)
		//	{
		//		if (point->vec[i] < mads->varLB->vec[i] || point->vec[i] > mads->varUB->vec[i])
		//		{
		//			// printf("currentPt is infeasible\n");
		//			exit(10);
		//		}
		//	}

		}
		else
		{
			mads->ptIndex=inputUniqueRepeatIndex[0];
			funcVal=mads->funcSet->vec[inputUniqueRepeatIndex[0]];
		}
//		 printf("printing inputUniqueRepeatIndex %d and its funcValue %lf along with mads->numElem %d and mads->numFeasibleElem %d\n", inputUniqueRepeatIndex[0], funcVal, mads->numElem, mads->numFeasibleElem);

	}
	free (inputUniqueRepeatIndex);
	return funcVal;
}
