/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _VECMATDECLARE_H
#define _VECMATDECLARE_H
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<stdbool.h> /* use C99 or above versions */
#include<float.h>
#include<limits.h>
#include<string.h>

#define INFS DBL_MAX
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)
#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

typedef struct
{
  int dimension;
  int *vec;
} vectorZ;

typedef struct
{
  int dimension;
  double *vec;
} vectorD;

typedef struct
{
  int rows;
  int cols;
  double *mat;
} matrixD;

/*Initialize vector and matrix */
void vectorInitZ (vectorZ *inputVec, int numElem);
void vectorInit(vectorD *inputVec, int numElem);
void matrixInit(matrixD *inputMat, int rows, int cols);
void matrixInitFromMatrix(matrixD *inputMat, matrixD *A);

/*Find value and index of maximum and minimum element */
void vectorMax(vectorD *inputVec, double *maxVal, int *maxIndex);
void vectorMin(vectorD *inputVec, double *minVal, int *minIndex);
void matrixMax(matrixD *inputMat, double *maxVal, int *maxIndex, int *rowNum, int *colNum);
void matrixMin(matrixD *inputMat, double *minVal, int *minIndex, int *rowNum, int *colNum);

/* Print vector and matrix in pretty format */
void printVectorZ (vectorZ *vectorA);
void printVector (vectorD *vectorA);
void printVectorL (vectorD *vectorA, int numElem);
void printMatrix (matrixD *matrixA);
void printMatrixL (matrixD *matrixA, int rows, int cols);

/* Intialize to ones for vectors and matrix */
void vecOnesD (vectorD *vector, const int dimension);
void matOnesD (matrixD *matrix, const int rows, const int cols);

/* Initialize to zeros for vectors and matrix */
void vecZerosZ (vectorZ *vector, const int dimension);
void vecZerosD (vectorD *vector, const int dimension);
void matZerosD (matrixD *matrix, const int rows, const int cols);

/* Intialize to e-vector (vector of zeros except one element with value=1) or identity matrix */
void vecEyeZ (vectorZ *vector, const int elemNum, const int dimension);
void vecEyeD (vectorD *vector, const int elemNum, const int dimension);
void matEyeD (matrixD *matrix, const int rows, const int cols);

/* Create diagonal matrix from vector */
void diagMatrix (vectorD *inputVec, matrixD *outputMat);
void diagMatrixInverse (vectorD *inputVec, matrixD *outputMat);

/* Extract an element a (x) from vector "a" or A (x,y) from matrix "A" */
double elemExtractVecD (const vectorD *vector, const int elemNum);
double elemExtractMatD (const matrixD *matrix, const int x, const int y);

/* Transpose a matrix */
void matrixTranspose (matrixD *inputMat, matrixD *outputMat);
/* Copy upper or lower triangular elements to lower or upper part */
void upperLowerFull (char destination, matrixD *inputMat);

/* Extract column into vector */
void vecExtractMat (vectorD *vector, matrixD *matrix, int colnum);
void mat2Dextract (matrixD *matrix, vectorD **vectorCollectStruct);
void submatrixExtract (matrixD *inputMat, int *colIndex, int colElem, matrixD *outputMat);

/* Append vectors and matrices */

void vectorVectorAppend (vectorD *vec1, vectorD *vec2, vectorD *outputVec);
void matrixVectorAppend (char position, matrixD *inputMat, vectorD *inputVec);
void matrixMatrixHorizAppend (matrixD *matrix1, matrixD *matrix2);
void matrixMatrixVertAppend (matrixD *matrix1, matrixD *matrix2, matrixD *outputMat);
void matrixVectorVertAppend (char position, matrixD *inputMat, vectorD *inputVec, matrixD *outputMat);

/* Change from vector to matrix and vice versa */
void vectorMatrixConvert(char type, vectorD *vectorA, matrixD *matrixA);

/* Free the memory */
void freeVectorD (vectorD *vector);
void freeVectorZ (vectorZ *vector);
void freeMatrixD (matrixD *matrix);

#endif
