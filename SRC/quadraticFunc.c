/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "quadInterpolate.h"

//double quadFuncValue (vectorD *point, vectorD *mads->quadAlphaL, vectorD *mads->quadAlphaQ)
double quadFuncValue (madsStructure *mads, vectorD *point)
{
	int i,j;
	double funcLinear=mads->quadAlphaL->vec[0];
	for(i=1; i<mads->quadAlphaL->dimension; i++)
	{
		funcLinear +=mads->quadAlphaL->vec[i]*(point->vec[i-1]);
	}

	double funcQuad=0;
	for(i=0; i<point->dimension; i++)
	{
		funcQuad +=mads->quadAlphaQ->vec[i]*(pow(point->vec[i],2))*0.5;
	}

	int count=0;
	for(i=0; i<point->dimension-1; i++)
	{
		for(j=i+1; j<point->dimension; j++)
		{
			funcQuad +=point->vec[i]*(point->vec[j])*(mads->quadAlphaQ->vec[count+point->dimension]);
			count++;
		}
	}

	return funcLinear+funcQuad;
}


