/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "quadLinearHybrid.h"
#include <time.h>
//Argument 1 is starting point
//Argument 2 is dimension
//Argument 3 is max evals
//Argument 4 is poll step type
//Argument 5 is number of constraints
//Argument 6 is whether start point is feasible or not
//Argument 7 is problem number from fortran file
//Argument 8 is Bounds file

int main(int argc, char **argv)
{
	clock_t beginClock=clock();

	// printf ("testing function begins\n");
	userData *uData = malloc (sizeof(userData));
	userBasicIntialize (uData, argv);
	userExtraIntialize (uData, argv);
	int dimension=uData->dimension;
	int logLevel=uData->logLevel;
	//	vectorD *startPt=malloc(sizeof(vectorD));
	//	vectorInit (startPt, dimension);
	int i;

	//	FILE *fp;
	//	fp=fopen(argv[1],"r"); for (i=0; i<dimension; i++) {
	//		fscanf(fp,"%lf",&startPt->vec[i]);
	//	}
	//	fclose(fp);

	char objectiveType;
	double bestValueActual;
	int iterLimit=uData->iterLimit;
	bool quickSG = 0;
	char *seqType=uData->pollPattern;
	int numConstr=uData->numConstr;
	//bool startPtNewFeasibility=uData->initPtFeasibility;
	bool startPtNewFeasibility = true;

	double *varLB=malloc (sizeof(double)*dimension);
	double *varUB=malloc (sizeof(double)*dimension);
	double *constrLB=malloc (sizeof(double)*numConstr);
	double *constrUB=malloc (sizeof(double)*numConstr);


	boundsEval (varLB, varUB, constrLB, constrUB, uData);


	// printf("printing lower and upper bounds on variables \n");
	//	for (i=0; i<dimension; i++)
	//	{
	// printf ("%G\t%G\n",varLB[i],varUB[i]);
	//	}

	//	// printf ("printing lower and upper bounds on constraints\n");
	//	for (i=0; i<numConstr; i++)
	//	{
	//		// printf ("%G\t%G\n",constrLB[i],constrUB[i]);
	//	}

	double boxLengthMin=INFS;
	for (i=0; i<dimension; i++)
	{
		if (varUB[i]-varLB[i] > 1e-15)
		{
			if (varUB[i]-varLB[i] < boxLengthMin)
			{
				boxLengthMin=varUB[i]-varLB[i];
			}
		}
		else
		{
			printf ("%d variable is already fixed. Dimension of the problem must be reduced before moving further.\n",i);
			printf("Empty interior\n");
			exit(10);
		}
	}

	vectorD *startPtNew = malloc (sizeof(vectorD));
	vectorInit (startPtNew,dimension);
	memcpy (startPtNew->vec, uData->startPt, sizeof(double)*dimension);

	int numPollSteps=100;
	double radius= MIN (100,boxLengthMin/2);
	//	double precision=1E-6;
	double precision=uData->precision;

	bool feasibleSolutionFound;

	if (startPtNewFeasibility == false)
	{
		madsStructure *madsFeasibleSearch=malloc (sizeof(madsStructure));
		simplexGradStruct *sg=malloc (sizeof(simplexGradStruct));
		objectiveType='C';
		madsInit (madsFeasibleSearch, startPtNew, dimension, radius, precision, iterLimit*10, numPollSteps, varLB, varUB, numConstr, boxLengthMin/2, objectiveType);

		if (*seqType=='H')
		{
			haltonSeqGen (madsFeasibleSearch);
		}
	//	else if (*seqType=='S')
	//	{
	//		simplexSeqGen (madsFeasibleSearch);
	//	}
		else
		{
			// printf ("unknown sequence generator supplied. Setting to default simplexGenerator \n");
		}
		madsFeasibleSearch->bestVal=singlePtEval (startPtNew, madsFeasibleSearch, uData);
		if (madsFeasibleSearch->constrSet->vec[madsFeasibleSearch->bestPtIndex] < 1E-15)
		{
			feasibleSolutionFound = true;
			// printf ("found a feasible solution \n");
		}
		else
		{

			lineSearchStruct *gs=malloc (sizeof(lineSearchStruct));
			lineSearchInitiate (gs, startPtNew, startPtNew, 20, 9);
			simplexGradInit (madsFeasibleSearch, sg, quickSG);
			scgStructQL *conjGrad=malloc (sizeof(scgStructQL));
			scgInitQL (conjGrad, madsFeasibleSearch->dimension);
			quadScgMadsQL (madsFeasibleSearch, conjGrad, sg, gs, uData);

			freeLineSearch (gs);
			freeSCGQL (conjGrad);
			freeSimplexGrad (sg);
			// printf ("End of search for feasible solution\n");
			bestValueActual=madsFeasibleSearch->funcSetActual->vec[madsFeasibleSearch->bestPtIndex];
			// printf ("best function value is %G\n",bestValueActual);
			// printf ("combined constraint value i.e. sum_i (max (0, g_i(x))) at best point is %G\n",madsFeasibleSearch->constrSet->vec[madsFeasibleSearch->bestPtIndex]);
			// printf ("printing penalty parameter at the end %G\n",madsFeasibleSearch->penaltyParam);
		}

		if (madsFeasibleSearch->constrSet->vec[madsFeasibleSearch->bestPtIndex] < 1E-15)
		{
			feasibleSolutionFound = true;
			// printf ("found a feasible solution \n");
		}
		else
		{
			feasibleSolutionFound = false;
			// printf ("no feasible solution found. Terminating the program\n");
			exit (10);
		}
		copyVectors (madsFeasibleSearch->bestPt, startPtNew);
	}


	if (startPtNewFeasibility == true || feasibleSolutionFound == true)
	{
		// printf ("Starting with a feasible solution\n");
		madsStructure *madsOptSearch=malloc (sizeof(madsStructure));
		objectiveType='O';
		madsInit (madsOptSearch, startPtNew, dimension, radius, precision, iterLimit, numPollSteps, varLB, varUB, numConstr, boxLengthMin/2, objectiveType);
		if (*seqType=='H')
		{
			haltonSeqGen (madsOptSearch);
		}
	//	else if (*seqType=='S')
	//	{
	//		simplexSeqGen (madsOptSearch);
	//	}
		else
		{
			// printf ("unknown sequence generator supplied. Setting to default simplexGenerator \n");
		}
		madsOptSearch->bestVal=singlePtEval (startPtNew, madsOptSearch, uData);
		simplexGradStruct *sgObjective=malloc (sizeof(simplexGradStruct));
		lineSearchStruct *gsObjective=malloc (sizeof(lineSearchStruct));
		lineSearchInitiate (gsObjective, startPtNew, startPtNew, 20, 9);
		simplexGradInit (madsOptSearch, sgObjective, quickSG);
		scgStructQL *conjGradObjective=malloc (sizeof(scgStructQL));
		scgInitQL (conjGradObjective, madsOptSearch->dimension);
		quadScgMadsQL (madsOptSearch, conjGradObjective, sgObjective, gsObjective, uData);

		//		 printf ("final vector is \n");
		//		printVector (madsOptSearch->bestPt);

		double bestFuncVal=DBL_MAX;
		int bestIndex=-1;
		for (i=0; i < madsOptSearch->numFeasibleElem; i++)
		{
			if (madsOptSearch->ptSetSoftFeasibility[i] == 'F')
			{
				if (madsOptSearch->funcSetActual->vec[i] <= bestFuncVal)
				{
					bestFuncVal = madsOptSearch->funcSetActual->vec[i];
					bestIndex = i;
				}
			}
		}

		bestValueActual=madsOptSearch->funcSetActual->vec[madsOptSearch->bestPtIndex];
		// printf ("printing penalty parameter at the end %G\n",madsOptSearch->penaltyParam);
		// printf ("best function value is %G\n",bestValueActual);
		// printf ("combined constraint value i.e. sum_i (max (0, g_i(x))) at best point is %G\n",madsOptSearch->constrSet->vec[bestIndex]);
		if (bestIndex != -1)
		{
			printf ("best function value is %e\n", bestFuncVal);
		}
		else
		{
			// printf ("all points failed to satify constraints \n");
		}
		printf ("Number of feasible function evals done is %d\n", madsOptSearch->numFeasibleElem);
		fclose(madsOptSearch->funcList);
		printf ("printing final solution\n");
		printVector (madsOptSearch->bestPt);

		if (logLevel == 1 || logLevel == 2)
		{
			vectorD *vectorA = malloc (sizeof(vectorD));
			vectorInit (vectorA,madsOptSearch->numFeasibleElem);
			memcpy (vectorA->vec, madsOptSearch->funcSetActual->vec, sizeof(double)*(vectorA->dimension));

			FILE *fpVector;
			fpVector=fopen("FeasibleSolutions.csv","w");
			int i;
			for (i=0; i<vectorA->dimension; i++)
			{
				fprintf (fpVector,"%.7f\n",vectorA->vec[i]);
			}
			fclose (fpVector);
		}

		if (logLevel == 2)
		{
			matrixD *matrixA = malloc (sizeof(matrixD));
			matrixInit (matrixA, dimension, madsOptSearch->numFeasibleElem);
			memcpy (matrixA->mat, madsOptSearch->ptSet->mat, sizeof(double)*dimension*(madsOptSearch->numFeasibleElem));
			FILE *fpMatrix;
			fpMatrix=fopen("FeasiblePoints.csv","w");
			int i, j;
			int rows=matrixA->rows;
			int cols=matrixA->cols;
			for (i=0; i<rows; i++)
			{
				for (j=0; j<cols-1; j++)
				{
					fprintf (fpMatrix,"%.7f,", matrixA->mat[i+j*rows]);
				}
				fprintf (fpMatrix,"%.7f", matrixA->mat[i+(cols-1)*rows]);
				fprintf (fpMatrix,"\n");
			}
			fclose (fpMatrix);
		}
	}

	clock_t endClock=clock();
	double timeSpent=(double)(endClock-beginClock)/CLOCKS_PER_SEC;
	printf ("printing time taken by algorithm %f\n",timeSpent);
	//	// printf ("printing bounds at the end\n");
	//	for (i=0; i<dimension; i++)
	//	{
	//		// printf("%lf\t%lf\n",varLB[i],varUB[i]);
	//	}
	return 0;
}
