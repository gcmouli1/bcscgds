/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#include "quadInterpolate.h"

//void quadMatrix (matrixD *ptsMatrix, vectorD *mads->interpolateFuncVal, vectorD *mads->quadAlphaL, vectorD *mads->quadAlphaQ)
//Make sure to filter ptsMatrix from ptSet so that distant points are not included
void quadMatrix (madsStructure *mads)
{
	int numPts=mads->numIterpolatePts;
	// printf ("printing no. of points sent for iterpolation=%d and actual no. of points=%d\n",numPts,mads->numElem);
	matrixD *ptsMatrix=malloc (sizeof(matrixD));
	matrixInit (ptsMatrix, mads->dimension, numPts);
	memcpy (ptsMatrix->mat, mads->ptsInterpolate->mat, sizeof(double)*numPts*mads->dimension);
//	// printf("printing ptsMatrix in quad iterpolation\n");
//	printMatrix (ptsMatrix);
	vectorD *funcEvalPts=malloc (sizeof(vectorD));
	vectorInit (funcEvalPts, numPts);
	memcpy (funcEvalPts->vec, mads->interpolateFuncVal->vec, sizeof(double)*numPts);
	int dimension=ptsMatrix->rows;
	int nQ=dimension * (dimension+1)/2;
	int rows=ptsMatrix->rows;
	matrixD *linearPart=malloc (sizeof (matrixD));
	matrixInit (linearPart, ptsMatrix->cols, ptsMatrix->rows);
	matrixTranspose (ptsMatrix, linearPart);
	vectorD *vecOnes=malloc (sizeof (vectorD));
	vecOnesD (vecOnes, numPts);
	matrixVectorAppend ('F', linearPart, vecOnes);

	/* Our implementation of quadratic M (phi_Q) is different from paper. We store its transpose */
	matrixD *quadPart=malloc (sizeof (matrixD));
	matrixInit (quadPart, nQ, numPts);

	int i, j, k;  //, tmpIndex;
	int count=0;
	for (k=0; k<numPts; k++)
	{
		for(i=0; i<rows; i++)
		{
			quadPart->mat[i+k*nQ]=0.5*pow(ptsMatrix->mat[i+k*rows],2);
		}
	}

	for (k=0; k<numPts; k++)
	{
		for (i=0; i<rows; i++)
		{
			for (j=i+1; j<rows; j++)
			{
				quadPart->mat[count+rows*(k+1)]=(ptsMatrix->mat[i+k*rows])*(ptsMatrix->mat[j+k*rows]);
				count++;
			}
		}
	}

	//	// printf ("Printing quadPart matrix\n");
	//	printMatrix (quadPart);

	int numPolyDegreePts=(dimension+1)*(dimension+2)/2;
	if(numPts >= numPolyDegreePts)
	{
		/* Solving least Square problem of overdetermined linear system */
		freeVectorD (vecOnes);
		matrixD *quadPartTrans=malloc (sizeof(matrixD));
		matrixInit (quadPartTrans, numPts, nQ);
		matrixTranspose (quadPart, quadPartTrans);
		matrixMatrixHorizAppend (linearPart, quadPartTrans);
		vectorD *alpha=malloc (sizeof (vectorD));
		vectorInit (alpha, numPolyDegreePts);
	//	// printf ("printing pts matrix \n");
	//	printMatrix (ptsMatrix);
		// printf ("Overdetermined system. Using least square\n");
		// printf ("printing rows and cols of matrix for least square %d %d\n", linearPart->rows, linearPart->cols);
		// printf ("num. of elements in alpha %d\n",alpha->dimension);
		// printf ("num. of elements in funcEvalPts %d\n",funcEvalPts->dimension);
	//	// printf ("printing linear part \n");
	//	printMatrix (linearPart);
	//	// printf ("printing funcEvalPts \n");
		//printVector (funcEvalPts);

		leastSquare ('N', linearPart, funcEvalPts, alpha);
		memcpy(mads->quadAlphaL->vec, alpha->vec, sizeof(double)*(dimension+1));
		memcpy(mads->quadAlphaQ->vec, alpha->vec+dimension+1, sizeof(double)*nQ);
	//	// printf("printing alphaL part\n");
		//printVector (mads->quadAlphaL);
	//	// printf("printing alphaQ part\n");
	//	printVector (mads->quadAlphaQ);
		freeVectorD(alpha);
		freeMatrixD(linearPart);
		freeMatrixD(quadPart);
		freeMatrixD(quadPartTrans);
		freeMatrixD (ptsMatrix);
		freeVectorD (funcEvalPts);
	}
	else
	{
		/* Solving Big Quadratic Matrix */

		matrixD *linearPartTrans=malloc (sizeof (matrixD));
		matrixInit (linearPartTrans, ptsMatrix->rows+1, ptsMatrix->cols);
		matrixVectorVertAppend ('T', ptsMatrix, vecOnes, linearPartTrans);
		freeVectorD (vecOnes);

		matrixD *zeroMat=malloc (sizeof (matrixD));
		matZerosD (zeroMat, dimension+1, dimension+1);
		matrixD *quadquadT=malloc (sizeof (matrixD));
		matrixInit (quadquadT, quadPart->cols, quadPart->cols);
		matrixMultiply ('T', 'N', 1.0, quadPart, quadPart, quadquadT);
	//	// printf ("Printing linearPart matrix \n");
	//	printMatrix (linearPart);
	//	// printf ("Printing linearPartTrans matrix \n");
	//	printMatrix (linearPartTrans);
	//	// printf ("Printing quadquadT matrix \n");
	//	printMatrix (quadquadT);
		matrixMatrixHorizAppend (quadquadT, linearPart);
		matrixMatrixHorizAppend (linearPartTrans, zeroMat);
		matrixD *QuadBigMatrix=malloc (sizeof (matrixD));
		matrixInit (QuadBigMatrix, dimension+numPts+1, dimension+numPts+1);
		matrixMatrixVertAppend (quadquadT, linearPartTrans, QuadBigMatrix);
		freeMatrixD (zeroMat);
		freeMatrixD (quadquadT);
		freeMatrixD (linearPart);
		freeMatrixD (linearPartTrans);
		vectorD *vecZeros=malloc (sizeof (vectorD));
		vecZerosD (vecZeros, dimension+1);
		vectorD *rhs=malloc (sizeof (vectorD));
		vectorInit (rhs, numPts+dimension+1);
		vectorVectorAppend (funcEvalPts, vecZeros, rhs);
		//// printf ("Printing funcEvalPts \n");
		//printVector (funcEvalPts);
	//	// printf ("Printing rhs \n");
	//	printVector (rhs);
		int info;
		vectorD *coefficients=malloc (sizeof (vectorD));
		vectorInit (coefficients, QuadBigMatrix->cols);
		linearSystem (QuadBigMatrix, rhs, coefficients, &info); 
		if (info!=0)
		{
			int rowcolmin=QuadBigMatrix->rows;
			if (rowcolmin> (QuadBigMatrix->cols))
			{
				rowcolmin=QuadBigMatrix->cols;
			}
			matrixD *U=malloc (sizeof (matrixD));
			matrixInit (U, QuadBigMatrix->rows, QuadBigMatrix->rows);
			matrixD *VT=malloc (sizeof (matrixD));
			matrixInit (VT, QuadBigMatrix->cols, QuadBigMatrix->cols);
			vectorD *S=malloc (sizeof (vectorD));
			vectorInit (S,rowcolmin);
			matrixSVDD (QuadBigMatrix, U, S, VT);
			//// printf("printing U matrix \n");
			//printMatrix (U);
			//// printf("printing S vector \n");
			//printVector (S);
			//// printf("printing VT matrix \n");
			//printMatrix (VT);
			//// printf("printing QuadBigMatrix \n");
			//printMatrix (QuadBigMatrix);
			freeMatrixD (QuadBigMatrix);
			double threshhold_low=1e-15;
			double threshhold_high=1e-5;

			bool negSingularVal=0;
			int i;
			for (i=0; i<rowcolmin; i++)
			{
				if (S->vec[i]<=threshhold_low)
				{
					negSingularVal=1;
					S->vec[i]=threshhold_high;
				}
			}
			if (negSingularVal==true)
			{
				// printf("there exist negative singular values\n");
			}
			matrixD *Sigma=malloc (sizeof (matrixD));
			matZerosD (Sigma, S->dimension, S->dimension);
			diagMatrix (S, Sigma);
			matrixD *tmpMultiply=malloc (sizeof (matrixD));
			matrixInit (tmpMultiply, U->rows, Sigma->cols);
			matrixMultiply ('N', 'N', 1.0, U, Sigma, tmpMultiply);
			freeMatrixD (U);
			freeMatrixD (Sigma);
			free (S);
			matrixD *SVDPositive=malloc (sizeof (matrixD));
			matrixInit (SVDPositive, dimension+numPts+1, dimension+numPts+1);

			matrixMultiply ('N', 'N', 1.0, tmpMultiply, VT, SVDPositive);
			//// printf ("printing SVDPositive matrix\n");
			//printMatrix (SVDPositive);
			freeMatrixD (tmpMultiply);
			freeMatrixD (VT);

			//		vectorD *coefficients=malloc (sizeof (vectorD));
			//		vectorInit (coefficients, SVDPositive->cols);
			//	// printf("svd rows=%d, cols=%d, and num elements in rhs=%d and coefficients=%d\n", SVDPositive->rows, SVDPositive->cols, rhs->dimension, coefficients->dimension);
			linearSystem (SVDPositive, rhs, coefficients, &info); 
			//// printf("Printing solution of big matrix linear system\n");
			//printVector (coefficients);
			//leastSquare ('N', SVDPositive, rhs, coefficients);
			freeMatrixD (SVDPositive);
			freeVectorD (rhs);
		}

		/* Computing mu and alpha_Q as defined in paper */
		vectorD *mu=malloc (sizeof (vectorD));
		vectorInit (mu, numPts);
		memcpy(mu->vec, coefficients->vec, sizeof(double)*numPts);
		memcpy(mads->quadAlphaL->vec, coefficients->vec+numPts, sizeof(double)*(dimension+1));
		freeVectorD (coefficients);
		vectorMatrixMultOrig ('N', 1.0, quadPart, mu, 0.0, mads->quadAlphaQ);
		//	// printf("printing alphaL part\n");
		//printVector (mads->quadAlphaL);
		//// printf("printing alphaQ part\n");
		//printVector (mads->quadAlphaQ);
		freeVectorD (mu);
		freeMatrixD (quadPart);
		freeMatrixD (ptsMatrix);
		freeVectorD (funcEvalPts);
	}
}

double quadFuncValue (madsStructure *mads, vectorD *point)
{
	//	// printf("starting quadFuncValue\n");
	//	// printf ("%lf\n",mads->quadAlphaL->vec[0]);
	//	// printf ("%lf\n",mads->quadAlphaL->vec[mads->quadAlphaL->dimension-1]);
	//	// printf ("%lf\n",mads->quadAlphaQ->vec[0]);
	//	// printf ("%lf\n",mads->quadAlphaQ->vec[mads->quadAlphaQ->dimension-1]);
	//// printf ("printing alphaL \n");
	//printVector (mads->quadAlphaL);
	//// printf ("printing alphaQ\n");
	//printVector (mads->quadAlphaQ);
	int i,j;
	double funcLinear=mads->quadAlphaL->vec[0];
	for(i=1; i<mads->quadAlphaL->dimension; i++)
	{
		funcLinear +=mads->quadAlphaL->vec[i]*(point->vec[i-1]);
	}

	double funcQuad=0;
	for(i=0; i<point->dimension; i++)
	{
		funcQuad +=mads->quadAlphaQ->vec[i]*(pow(point->vec[i],2))*0.5;
	}

	int count=0;
	for(i=0; i<point->dimension-1; i++)
	{
		for(j=i+1; j<point->dimension; j++)
		{
			funcQuad +=point->vec[i]*(point->vec[j])*(mads->quadAlphaQ->vec[count+point->dimension]);
			count++;
		}
	}

	return funcLinear+funcQuad;
}

