/*
This file is a part of "bcscgds".
"bscgds" is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

"bscgds" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

/*
*  bcscgds: Bound Constrained Scaled Conjugate Gradient based Direct Search
*  Solver for Bound Constrained Derivative Free Optimization
*  author: Gannaravarapu Chandramouli
*/



#ifndef _MADSDECLARE_H_
#define _MADSDECLARE_H_
#include<time.h>
#include "vecLapack.h"
#include "primeCollect.h"

typedef struct
{
	char objectiveType;
	FILE *funcList;
	int numVicinityPts;
	int numVicinityPtsMax;
	int numPollSteps;
	bool pollStepLimitCrossed;
	double boxRadius;
	double radiusRebootPrecision;
	double diffLSbestValuePrecision;
	double penaltyParam;
	int numConstr;
	double constrValue;
	double radiusLimit;
	int dimension;
	double radius;
	double radiusInit;
	double svdTolerance;
	double bestVal;
	vectorD *bestPt;
	double *constrVec;
	vectorD *exactPenaltyConstr;
	int ptIndex;
	int bestPtIndex;
	int centerIndex;
	double negGradFuncValue;
	bool gradDescent;
	bool allbadPts;
	bool allWorsePts;
	bool badDistribution;
	double centralDistance;
	matrixD *centroidCollect;
	matrixD *haltonSequence;
//	matrixD *sobolSequence;
	vectorD *center;
	matrixD *ptSet;
	vectorD *funcSet;
	vectorD *funcSetActual;
	vectorD *constrSet;
	vectorD *ptSquare;
	double madsRadius;
	vectorD *vecOnes;
	vectorD *vecOnesNP1;
	matrixD *normSpanVec;
	vectorD *vecOnesSM;
	matrixD *searchMatrix;
	matrixD *searchMatrixOrigin;
	int *funcIndex;
	int iterLimit;
	int iterLimitActual;
	bool iterLimitCrossed;
	double madsRecFactor1;
	double madsRecFactor2;
	double meshLenRedFactor;
	vectorD *vecHash1;
	vectorD *vecHash2; 
	vectorD *vecHash3; 
	vectorD *ptSetHash1; 
	vectorD *ptSetHash2;
	vectorD *ptSetHash3;
	int numElem;
	int numFeasibleElem;
	int numInfeasibleElem;
	bool ptInfeasible;
	char *ptSetSoftFeasibility;
	bool *ptSetHardFeasibility;
	vectorD *funcVector;
	matrixD *singlePointMatrix;
	vectorD *tmpVec;
	double *tmpPt;
	vectorD *rotatePt;
	vectorD *gradient;
	vectorD *negGrad;
	vectorD *vTcFull;
	vectorD *simplexFuncValues;
	vectorZ *pointType;
	int numLineSearch;
	bool lineSearchPoint;
	vectorD *varLB;
	vectorD *varUB;
	vectorD *v1;
	vectorD *v2;
	int numEllipsoids;
	matrixD *ellipsoidRadiusMatrix;
	vectorD *ellipsoidScale;
	matrixD *ptsInterpolate;
	vectorD *interpolateFuncVal;
	vectorD *funcEvalPtsTmp;
	int numIterpolatePts;
	int numIneqConstrQuad;
	int numEqConstrQuad;
	vectorD *quadAlphaL;
	vectorD *quadAlphaQ;
	matrixD *quadHessian;
	vectorD *quadLinear;
	bool quadFitPSD;
	vectorD *direction;

} madsStructure;

void madsInit (madsStructure *mads, vectorD *startPt, int dimension, double radius, double precision, int iterLimit, int numPollSteps, double *varLB, double *varUB, int numConstr, double boxLengthMin, char objectiveType);

void freeMads(madsStructure *mads);
#endif
